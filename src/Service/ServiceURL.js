export const BASE_URL = "https://qlhd.haui.bfd.vn/api/v1/";
export const BASE_URL_IMAGE = "https://qlhd.haui.bfd.vn/api/";

//!User
export const Login = "auth/login/";
export const User = "users";
export const ChangePass = "auth/change-password";
export const ForgotPass = "auth/forgot-password";
export const ResetPass = "auth/reset-password";

//!Event
export const Event = "event";

//!
export const Team = "team";

//!Upload
const Upload = "upload";
const importUser = "upload/importUser";

export const Product = "product";
export const ProductByUser = "product/getProductByUser";
export const ProductType = "product/getProductType";
export const Unit = "product/getProductUnit";
export const Level = "product/getProductLevel";

export const UploadImage = "upload";

export const EventJoin = "eventJoin";
export const joinEvent = "eventJoin/join";

export const getDoor = "door";
export const drawDoor = "door/draw";

export default {
  BASE_URL,
  BASE_URL_IMAGE,
  Login,
  User,
  ChangePass,
  ForgotPass,
  ResetPass,

  Event,

  EventJoin,
  joinEvent,

  Team,

  Upload,
  importUser,

  Product,
  ProductByUser,
  ProductType,
  Unit,
  Level,

  UploadImage,

  getDoor,
  drawDoor,
};
