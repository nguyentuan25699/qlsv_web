/*!

=========================================================
* Material Dashboard React - v1.9.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2020 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
import EventIcon from "@material-ui/icons/Event";
// core components/views for Admin layout
import DashboardPage from "ViewsNew/Dashboard/Dashboard.js";
import UserProfile from "views/UserProfile/UserProfile.js";
import TableList from "views/TableList/TableList.js";
import Typography from "views/Typography/Typography.js";
import Icons from "views/Icons/Icons.js";
import Maps from "views/Maps/Maps.js";
import PeopleIcon from "@material-ui/icons/People";
import NotificationsPage from "views/Notifications/Notifications.js";
import UpgradeToPro from "views/UpgradeToPro/UpgradeToPro.js";
import EventManage from "ViewsNew/EventManage/EventManage.js";
import EventAvailableIcon from "@material-ui/icons/EventAvailable";
import LocalAtmIcon from "@material-ui/icons/LocalAtm";
// core components/views for RTL layout
import RTLPage from "views/RTLPage/RTLPage.js";
import UserManage from "ViewsNew/UserManage/UserManage";
import ScoreManage from "ViewsNew/ScoreManage/ScoreManage";
import TeamManage from "ViewsNew/TeamManage/TeamManage";
import FeesManage from "ViewsNew/FeesManage/FeesManage";
import UserManageAdd from "ViewsNew/UserManage/UserManageAdd";
import Profile from "ViewsNew/Profile/Profile";

const dashboardRoutes = [
  //!======================================
  {
    path: "/dashboard",
    name: "Dashboard",
    // rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/main",
  },
  {
    path: "/event-manage",
    name: "Quản lí sự kiện",
    icon: EventIcon,
    component: EventManage,
    layout: "/main",
  },
  {
    path: "/score-manage",
    name: "Quản lí điểm hoạt động",
    icon: EventAvailableIcon,
    component: ScoreManage,
    layout: "/main",
  },
  {
    path: "/users-manage",
    name: "Quản lí tài khoản",
    icon: Person,
    component: UserManage,
    layout: "/main",
  },
  {
    path: "/team-manage",
    name: "Quản lí Đội - Nhóm",
    icon: PeopleIcon,
    component: TeamManage,
    layout: "/main",
  },
  {
    path: "/fees-manage",
    name: "Quản lí Đoàn phí - Hội phí",
    icon: LocalAtmIcon,
    component: FeesManage,
    layout: "/main",
  },
  {
    path: "/profile",
    name: "Tài khoản cá nhân",
    icon: Person,
    component: Profile,
    layout: "/main",
    hide: true,
  },
  {
    path: "/user/:userID",
    name: "Quản lí tài khoản",
    icon: Person,
    component: UserManageAdd,
    layout: "/main",
    hide: true,
  },
  //!======================================
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   rtlName: "لوحة القيادة",
  //   icon: Dashboard,
  //   component: DashboardPage,
  //   layout: "/main",
  // },

  // {
  //   path: "/table",
  //   name: "Table List",
  //   rtlName: "قائمة الجدول",
  //   icon: "content_paste",
  //   component: TableList,
  //   layout: "/main",
  // },
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   rtlName: "طباعة",
  //   icon: LibraryBooks,
  //   component: Typography,
  //   layout: "/main",
  // },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   rtlName: "الرموز",
  //   icon: BubbleChart,
  //   component: Icons,
  //   layout: "/main",
  // },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   rtlName: "خرائط",
  //   icon: LocationOn,
  //   component: Maps,
  //   layout: "/main",
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/main",
  // },
  // {
  //   path: "/rtl-page",
  //   name: "RTL Support",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,
  //   component: RTLPage,
  //   layout: "/rtl",
  // },
  // {
  //   path: "/upgrade-to-pro",
  //   name: "Upgrade To PRO",
  //   rtlName: "التطور للاحترافية",
  //   icon: Unarchive,
  //   component: UpgradeToPro,
  //   layout: "/main",
  // },
];

export default dashboardRoutes;
