import defaultAvatar from "assets/img/placeholder.jpg";
// core components
import Button from "components/CustomButtons/Button.js";
import _ from "lodash";
// used for making the prop types of this component
import PropTypes from "prop-types";
import React from "react";
//Icon
import CloseIcon from "@material-ui/icons/Close";
import DoneIcon from "@material-ui/icons/Done";

function CSVUpload(props) {
  const [file, setFile] = React.useState(null);
  const [imagePreviewUrl, setImagePreviewUrl] = React.useState(defaultAvatar);
  React.useEffect(() => {
    if (props.avatar !== null && !_.isEmpty(props.avatar))
      setImagePreviewUrl(props.avatar);
  }, [props.avatar]);
  let fileInput = React.createRef();
  const handleFileChange = (e) => {
    e.preventDefault();

    let reader = new FileReader();
    let file = e.target.files[0];
    reader.onloadend = () => {
      setFile(file);
      setImagePreviewUrl(reader.result);
      if (props.onChange) {
        props.onChange(file);
      }
    };
    reader.readAsDataURL(file);
  };

  React.useEffect(() => {
    let reader = new FileReader();
    reader.onloadend = () => {
      setFile(props.defaultAvatarCustom);
      setImagePreviewUrl(reader.result);
      //   if (props.onChange) {
      //     props.onChange(props.defaultAvatarCustom);
      //   }
    };
    // reader.readAsDataURL(props.defaultAvatarCustom);
  });
  // eslint-disable-next-line
  const handleSubmit = (e) => {
    // e.preventDefault();
    // file is the file/image uploaded
    // in this function you can save the image (file) on form submit
    // you have to call it yourself
    props.files(file);
  };
  const handleClick = () => {
    fileInput.current.click();
  };
  const handleRemove = () => {
    setFile(null);
    // setImagePreviewUrl(props.avatar ? props.avatar : defaultAvatar);
    fileInput.current.value = null;
  };
  let {
    avatar,
    addButtonProps,
    changeButtonProps,
    removeButtonProps,
    defaultAvatarCustom,
  } = props;
  return (
    <div className="fileinput text-center">
      <input
        style={{ display: "none" }}
        type="file"
        accept=".csv"
        onChange={handleFileChange}
        ref={fileInput}
      />
      {/* <div className={"thumbnail" + (avatar ? " img-square" : "")}>
        <img
          src={imagePreviewUrl}
          alt="..."
          style={{ maxWidth: "300px", maxHeight: "300px" }}
        />
      </div> */}
      <div
        className="fileinput text-center"
        style={{ display: "flex", justifyContent: "center" }}
      >
        <p>
          {file !== null
            ? "Tệp đã chọn: " + file.name
            : "Vui lòng chọn tệp định dạng .csv"}
        </p>
      </div>
      <div style={{ display: "flex", justifyContent: "center" }}>
        {file === null ? (
          <Button {...addButtonProps} onClick={() => handleClick()}>
            {/* {avatar ? "Add Photo" : "Select image"} */}
            Chọn file CSV
          </Button>
        ) : (
          <span>
            <Button
              {...changeButtonProps}
              color="success"
              onClick={() => {
                handleSubmit(file);
                setFile(null);
              }}
            >
              <DoneIcon />
              Lưu
            </Button>
            {/* {avatar ? <br /> : null} */}
            <Button {...removeButtonProps} onClick={() => handleRemove()}>
              <CloseIcon style={{ fontSize: "10px" }} /> Huỷ
            </Button>
          </span>
        )}
      </div>
    </div>
  );
}

export default CSVUpload;
