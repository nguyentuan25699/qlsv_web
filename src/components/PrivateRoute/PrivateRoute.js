import React from "react";
import { Redirect, Route } from "react-router-dom";

const PrivateRoute = ({ component: Component, ...rest }) => {
  // Add your own authentication on the below line.
  const [authLogin, setauthLogin] = React.useState(true);
  React.useEffect(() => {
    if (
      localStorage.getItem("token") &&
      localStorage.getItem("token") !== "" &&
      localStorage.getItem("role") !== "user"
    ) {
      setauthLogin(true);
    } else {
      setauthLogin(false);
    }
  });

  return (
    <Route
      {...rest}
      render={(props) =>
        authLogin ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        )
      }
    />
  );
};

export default PrivateRoute;
