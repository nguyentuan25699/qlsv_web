/*eslint-disable*/
import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";

import PropTypes from "prop-types";
// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import ListItem from "@material-ui/core/ListItem";
import List from "@material-ui/core/List";
// core components
import styles from "assets/jss/material-dashboard-react/components/footerStyle.js";

const useStyles = makeStyles(styles);

export default function Footer(props) {
  const classes = useStyles();
  const { children, content, theme, big, className } = props;
  const themeType =
    theme === "transparent" || theme == undefined ? false : true;
  const footerClasses = classNames({
    [classes.footer]: true,
    [classes[theme]]: themeType,
    [classes.big]: big || children !== undefined,
    [className]: className !== undefined,
  });

  return (
    <footer className={footerClasses} sty>
      <div className={classes.container}>
        {/* <div className={classes.left}> */}
        {/* <List className={classes.list}>
            <ListItem className={classes.inlineBlock}>
              <a href="#home" className={classes.block}>
                Home
              </a>
            </ListItem>
            <ListItem className={classes.inlineBlock}>
              <a href="#company" className={classes.block}>
                Company
              </a>
            </ListItem>
            <ListItem className={classes.inlineBlock}>
              <a href="#portfolio" className={classes.block}>
                Portfolio
              </a>
            </ListItem>
            <ListItem className={classes.inlineBlock}>
              <a href="#blog" className={classes.block}>
                Blog
              </a>
            </ListItem>
          </List> */}
        <p className={classes.left}>
          Hệ thống quản lý hoạt động Đoàn - Hội sinh viên
        </p>
        {/* </div> */}
        <p className={classes.right}>
          <span>
            &copy; {1900 + new Date().getYear()} Phát triển bởi{" "}
            <a
              href="https://www.facebook.com/nguyentuan6899"
              target="_blank"
              className={classes.a}
            >
              Nguyễn Tuấn
            </a>
          </span>
        </p>
      </div>
    </footer>
  );
}
