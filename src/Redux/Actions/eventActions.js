import TypeActions from "Redux/TypeActions";

export const getListEvent = (params, callback) => {
  return {
    type: TypeActions.GET_LIST_EVENT_REQUEST,
    params,
    callback,
  };
};

export const createEvent = (body, callback) => {
  return {
    type: TypeActions.CREATE_EVENT_REQUEST,
    body,
    callback,
  };
};

export const updateEvent = (body, params, callback) => {
  return {
    type: TypeActions.UPDATE_EVENT_REQUEST,
    body,
    params,
    callback,
  };
};

export const deleteEvent = (params, callback) => {
  return {
    type: TypeActions.DELETE_EVENT_REQUEST,
    params,
    callback,
  };
};

export default { getListEvent, createEvent, updateEvent, deleteEvent };
