// const { default: TypeActions } = require("Redux/TypeActions");

import TypeActions from "Redux/TypeActions";

export const login = (body, callback) => {
  return {
    type: TypeActions.LOGIN_REQUEST,
    body,
    callback,
  };
};

export const getUsers = (params) => {
  return {
    type: TypeActions.GET_USERS_REQUEST,
    params,
    // callback,
  };
};

export const deleteUsers = (params, callback) => {
  return {
    type: TypeActions.DELETE_USERS_REQUEST,
    params,
    callback,
  };
};

export const getListUsers = (params, callback) => {
  return {
    type: TypeActions.GET_LIST_USERS_REQUEST,
    params,
    callback,
  };
};

export const createUsers = (body, callback) => {
  return {
    type: TypeActions.CREATE_USERS_REQUEST,
    body,
    callback,
  };
};

export const editUsers = (body, params, callback) => {
  return {
    type: TypeActions.EDIT_USERS_REQUEST,
    body,
    params,
    callback,
  };
};

export const changePassword = (body, params, callback) => {
  return {
    type: TypeActions.CHANGE_PASSWORD_REQUEST,
    body,
    params,
    callback,
  };
};
export const forgotPassword = (body, callback) => {
  return {
    type: TypeActions.FORGOT_PASSWORD_REQUEST,
    body,
    callback,
  };
};

export const resetPassword = (body, params, callback) => {
  return {
    type: TypeActions.RESET_PASSWORD_REQUEST,
    body,
    params,
    callback,
  };
};

export const logout = (body, callback) => {
  return {
    type: TypeActions.LOG_OUT,
    body,
    callback,
  };
};

export default {
  login,
  getUsers,
  getListUsers,
  createUsers,
  editUsers,
  deleteUsers,
  changePassword,
  forgotPassword,
  resetPassword,
  logout,
};
