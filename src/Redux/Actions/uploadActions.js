import TypeActions from "Redux/TypeActions";

export const uploadImage = (body, callback) => {
  return {
    type: TypeActions.UPLOAD_IMAGE_REQUEST,
    body,
    callback,
  };
};
export const uploadFile = (body, callback) => {
  return {
    type: TypeActions.UPLOAD_FILE_REQUEST,
    body,
    callback,
  };
};

export default { uploadImage, uploadFile };
