import TypeActions from "Redux/TypeActions";

export const getListEventJoin = (params, callback) => {
  return {
    type: TypeActions.GET_LIST_EVENT_JOIN_REQUEST,
    params,
    callback,
  };
};

export const createEventJoin = (body, callback) => {
  return {
    type: TypeActions.CREATE_EVENT_JOIN_REQUEST,
    body,
    callback,
  };
};

export const updateEventJoin = (body, params, callback) => {
  return {
    type: TypeActions.UPDATE_EVENT_JOIN_REQUEST,
    body,
    params,
    callback,
  };
};

export const deleteEventJoin = (params, callback) => {
  return {
    type: TypeActions.DELETE_EVENT_JOIN_REQUEST,
    params,
    callback,
  };
};

export const joinEvent = (body, callback) => {
  return {
    type: TypeActions.JOIN_EVENT_REQUEST,
    body,
    callback,
  };
};

export default {
  getListEventJoin,
  createEventJoin,
  updateEventJoin,
  deleteEventJoin,
};
