import TypeActions from "Redux/TypeActions";

const initialState = {
  data: {},
  listUser: [],
  userByID: {},
  error: "",
  imageURL: "",
  errorLogin: "",
  isGetting: false,
  isLogging: false,
  isCreating: false,
  isEditting: false,
  isDeleting: false,
  isUploading: false,
  isChanging: false,
  isLoading: false,
  isReseting: false,
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    //!Get User
    case TypeActions.GET_USERS_REQUEST:
      return {
        ...state,
        isGetting: true,
      };
    case TypeActions.GET_USERS_SUCCESS:
      return {
        ...state,
        isGetting: false,
        userByID: action.data,
      };
    case TypeActions.GET_USERS_FAILED:
      return {
        ...state,
        isGetting: false,
        error: action.error,
      };
    //!Delete User
    case TypeActions.DELETE_USERS_REQUEST:
      return {
        ...state,
        isDeleting: true,
      };
    case TypeActions.DELETE_USERS_SUCCESS:
      return {
        ...state,
        isDeleting: false,
        error: "",
      };
    case TypeActions.DELETE_USERS_FAILED:
      return {
        ...state,
        isDeleting: false,
        error: action.error,
      };
    //!Get List User
    case TypeActions.GET_LIST_USERS_REQUEST:
      return {
        ...state,
        isGetting: true,
      };
    case TypeActions.GET_LIST_USERS_SUCCESS:
      return {
        ...state,
        isGetting: false,
        listUser: action.data,
        error: "",
      };
    case TypeActions.GET_LIST_USERS_FAILED:
      return {
        ...state,
        isGetting: false,
        error: action.error,
      };
    //!Create User
    case TypeActions.CREATE_USERS_REQUEST:
      return {
        ...state,
        isCreating: true,
      };
    case TypeActions.CREATE_USERS_SUCCESS:
      return {
        ...state,
        isCreating: false,
        error: "",
      };
    case TypeActions.CREATE_USERS_FAILED:
      return {
        ...state,
        isCreating: false,
        error: action.error,
      };
    //!Edit User
    case TypeActions.EDIT_USERS_REQUEST:
      return {
        ...state,
        isEditting: true,
      };
    case TypeActions.EDIT_USERS_SUCCESS:
      return {
        ...state,
        isEditting: false,
        error: "",
      };
    case TypeActions.EDIT_USERS_FAILED:
      return {
        ...state,
        isEditting: false,
        error: action.error,
      };
    //!Login
    case TypeActions.LOGIN_REQUEST:
      return {
        ...state,
        isLogging: true,
      };
    case TypeActions.LOGIN_REQUEST_SUCCESS:
      return {
        ...state,
        isLogging: false,
        errorLogin: "",
      };
    case TypeActions.LOGIN_REQUEST_FAILED:
      return {
        ...state,
        isLogging: false,
        errorLogin: action.error,
      };
    //!Change password
    case TypeActions.CHANGE_PASSWORD_REQUEST:
      return {
        ...state,
        isChanging: true,
      };
    case TypeActions.CHANGE_PASSWORD_SUCCESS:
      return {
        ...state,
        isChanging: false,
        error: "",
      };
    case TypeActions.CHANGE_PASSWORD_FAILED:
      return {
        ...state,
        isChanging: false,
        errorLogin: action.error,
      };
    //!Forgot password
    case TypeActions.FORGOT_PASSWORD_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case TypeActions.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        error: "",
      };
    case TypeActions.FORGOT_PASSWORD_FAILED:
      return {
        ...state,
        isLoading: false,
        errorLogin: action.error,
      };
    //!Reset password
    case TypeActions.RESET_PASSWORD_REQUEST:
      return {
        ...state,
        isReseting: true,
      };
    case TypeActions.RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        isReseting: false,
        error: "",
      };
    case TypeActions.RESET_PASSWORD_FAILED:
      return {
        ...state,
        isReseting: false,
        errorLogin: action.error,
      };
    //!Log out
    case TypeActions.LOG_OUT:
      return {
        ...state,
        data: {},
        listUser: [],
        userByID: {},
        error: "",
      };
    case TypeActions.LOG_OUT_SUCCESS:
      return {
        ...state,
      };
    case TypeActions.LOG_OUT_FAILED:
      return {
        ...state,
      };
    //!Default
    default:
      return {
        ...state,
      };
  }
};
export default userReducer;
