import TypeActions from "Redux/TypeActions";

const initialState = {
  isGetting: false,
  isCreating: false,
  isUpdating: false,
  isDeleting: false,
  listTeam: [],
  error: "",
};

export const teamReducer = (state = initialState, action) => {
  switch (action.type) {
    //!Get Door
    case TypeActions.GET_LIST_TEAM_REQUEST:
      return {
        ...state,
        isGetting: true,
      };
    case TypeActions.GET_LIST_TEAM_SUCCESS:
      return {
        ...state,
        isGetting: false,
        listTeam: action.data,
      };
    case TypeActions.GET_LIST_TEAM_FAILED:
      return {
        ...state,
        isGetting: false,
        error: action.error,
      };
    //!Create Team
    case TypeActions.CREATE_TEAM_REQUEST:
      return {
        ...state,
        isCreating: true,
      };
    case TypeActions.CREATE_TEAM_SUCCESS:
      return {
        ...state,
        isCreating: false,
      };
    case TypeActions.CREATE_TEAM_FAILED:
      return {
        ...state,
        isCreating: false,
        error: action.error,
      };
    //!Update Team
    case TypeActions.UPDATE_TEAM_REQUEST:
      return {
        ...state,
        isUpdating: true,
      };
    case TypeActions.UPDATE_TEAM_SUCCESS:
      return {
        ...state,
        isUpdating: false,
      };
    case TypeActions.UPDATE_TEAM_FAILED:
      return {
        ...state,
        isUpdating: false,
        error: action.error,
      };
    //!Delete Team
    case TypeActions.DELETE_TEAM_REQUEST:
      return {
        ...state,
        isDeleting: true,
      };
    case TypeActions.DELETE_TEAM_SUCCESS:
      return {
        ...state,
        isDeleting: false,
      };
    case TypeActions.DELETE_TEAM_FAILED:
      return {
        ...state,
        isDeleting: false,
        error: action.error,
      };
    //!Default
    default:
      return {
        ...state,
      };
  }
};
export default teamReducer;
