import TypeActions from "Redux/TypeActions";

const initialState = {
  isUploading: false,
  imageLink: "",
  error: "",
};

export const uploadReducer = (state = initialState, action) => {
  switch (action.type) {
    //!Upload image
    case TypeActions.UPLOAD_IMAGE_REQUEST:
      return {
        ...state,
        isUploading: true,
      };
    case TypeActions.UPLOAD_IMAGE_SUCCESS:
      return {
        ...state,
        isUploading: false,
        imageLink: action.data,
      };
    case TypeActions.UPLOAD_IMAGE_FAILED:
      return {
        ...state,
        isUploading: false,
        error: action.error,
      };
    //!Upload file
    case TypeActions.UPLOAD_FILE_REQUEST:
      return {
        ...state,
        isUploading: true,
      };
    case TypeActions.UPLOAD_FILE_SUCCESS:
      return {
        ...state,
        isUploading: false,
        // imageLink: action.data,
      };
    case TypeActions.UPLOAD_FILE_FAILED:
      return {
        ...state,
        isUploading: false,
        error: action.error,
      };

    //!Default
    default:
      return {
        ...state,
      };
  }
};
export default uploadReducer;
