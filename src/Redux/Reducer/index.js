import { combineReducers } from "redux";
import { userReducer } from "./userReducer";
import { productReducer } from "./productReducer";
import { doorReducer } from "./doorReducer";
import { eventReducer } from "./eventReducer";
import { uploadReducer } from "./uploadReducer";
import { teamReducer } from "./teamReducer";
import { eventJoinReducer } from "./eventJoinReducer";

const rootReducers = combineReducers({
  userReducer,
  productReducer,
  doorReducer,
  eventReducer,
  uploadReducer,
  teamReducer,
  eventJoinReducer,
});
export default rootReducers;
