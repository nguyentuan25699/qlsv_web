import TypeActions from "Redux/TypeActions";

const initialState = {
  isGetting: false,
  isCreating: false,
  isUpdating: false,
  isDeleting: false,
  isJoining: false,
  listEventJoin: [],
  error: "",
};

export const eventJoinReducer = (state = initialState, action) => {
  switch (action.type) {
    //!Get Door
    case TypeActions.GET_LIST_EVENT_JOIN_REQUEST:
      return {
        ...state,
        isGetting: true,
      };
    case TypeActions.GET_LIST_EVENT_JOIN_SUCCESS:
      return {
        ...state,
        isGetting: false,
        listEventJoin: action.data,
      };
    case TypeActions.GET_LIST_EVENT_JOIN_FAILED:
      return {
        ...state,
        isGetting: false,
        error: action.error,
      };
    //!Create Event
    case TypeActions.CREATE_EVENT_JOIN_REQUEST:
      return {
        ...state,
        isCreating: true,
      };
    case TypeActions.CREATE_EVENT_JOIN_SUCCESS:
      return {
        ...state,
        isCreating: false,
      };
    case TypeActions.CREATE_EVENT_JOIN_FAILED:
      return {
        ...state,
        isCreating: false,
        error: action.error,
      };
    //!Update Event
    case TypeActions.UPDATE_EVENT_JOIN_REQUEST:
      return {
        ...state,
        isUpdating: true,
      };
    case TypeActions.UPDATE_EVENT_JOIN_SUCCESS:
      return {
        ...state,
        isUpdating: false,
      };
    case TypeActions.UPDATE_EVENT_JOIN_FAILED:
      return {
        ...state,
        isUpdating: false,
        error: action.error,
      };
    //!Delete Event
    case TypeActions.DELETE_EVENT_JOIN_REQUEST:
      return {
        ...state,
        isDeleting: true,
      };
    case TypeActions.DELETE_EVENT_JOIN_SUCCESS:
      return {
        ...state,
        isDeleting: false,
      };
    case TypeActions.DELETE_EVENT_JOIN_FAILED:
      return {
        ...state,
        isDeleting: false,
        error: action.error,
      };
    //!Join Event
    case TypeActions.JOIN_EVENT_REQUEST:
      return {
        ...state,
        isJoining: true,
      };
    case TypeActions.JOIN_EVENT_SUCCESS:
      return {
        ...state,
        isJoining: false,
      };
    case TypeActions.JOIN_EVENT_FAILED:
      return {
        ...state,
        isJoining: false,
        error: action.error,
      };
    //!Default
    default:
      return {
        ...state,
      };
  }
};
export default eventJoinReducer;
