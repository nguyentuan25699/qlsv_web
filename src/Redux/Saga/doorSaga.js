import _ from "lodash";
import { call, put, takeLatest } from "redux-saga/effects";
import TypeActions from "Redux/TypeActions";
import ServiceURL from "Service/ServiceURL";
import { GET } from "../../Service/ServiceBase";
// import axiosClientPdf from "../../Service/axiosClient";

const { axiosClientPdf } = require("../../Service/axiosClient");

const axios = require("axios");

export function* getDoors(data) {
  const url = ServiceURL.getDoor;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_DOOR_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_DOOR_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_DOOR_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* drawDoor(data) {
  // const callback = data.callback;
  const url = ServiceURL.drawDoor + "?" + data.body;
  try {
    // const res = yield call(GET, url);
    axiosClientPdf
      .get(url)
      .then((res) => {
        if (res.message && !_.isEmpty(res.message)) {
          console.log("err", res.message);
          // yield put({
          //   type: TypeActions.DRAW_DOOR_FAILED,
          //   error: res.message,
          // });
        } else {
          const pdfBlob = new Blob([res.data], { type: "application/pdf" });
          // saveAs(pdfBlob, "newPdf.pdf");
          // callback && callback.success(pdfBlob);
          // console.log('data', res.data);
          // yield put({
          //   type: TypeActions.DRAW_DOOR_SUCCESS,
          //   data: res.data,
          // });
        }
      })
      .catch((ex) => {
        console.log("error", ex);
      });
  } catch (error) {
    yield put({ type: TypeActions.DRAW_DOOR_FAILED, error });
  }
}

export default function* userSaga() {
  yield takeLatest(TypeActions.GET_DOOR_REQUEST, getDoors);
  yield takeLatest(TypeActions.DRAW_DOOR_REQUEST, drawDoor);
}
