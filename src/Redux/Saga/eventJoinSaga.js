import _ from "lodash";
import { call, put, takeLatest } from "redux-saga/effects";
import TypeActions from "Redux/TypeActions";
import ServiceURL from "Service/ServiceURL";
import { DELETE, GET, PATCH, POST } from "../../Service/ServiceBase";

export function* getListEventJoin(data) {
  const url = ServiceURL.EventJoin + "?" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_EVENT_JOIN_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_EVENT_JOIN_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success(res.data);
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_LIST_EVENT_JOIN_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* createEventJoin(data) {
  const callback = data.callback;
  const url = ServiceURL.EventJoin;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.CREATE_EVENT_JOIN_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.CREATE_EVENT_JOIN_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.CREATE_EVENT_JOIN_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* joinEvent(data) {
  const callback = data.callback;
  const url = ServiceURL.joinEvent;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.JOIN_EVENT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.JOIN_EVENT_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.JOIN_EVENT_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* deleteEventJoin(data) {
  const url = ServiceURL.EventJoin + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(DELETE, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.DELETE_EVENT_JOIN_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.DELETE_EVENT_JOIN_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.DELETE_EVENT_JOIN_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* updateEventJoin(data) {
  const url = ServiceURL.EventJoin + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(PATCH, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.UPDATE_EVENT_JOIN_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.UPDATE_EVENT_JOIN_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.UPDATE_EVENT_JOIN_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export default function* eventSaga() {
  yield takeLatest(TypeActions.GET_LIST_EVENT_JOIN_REQUEST, getListEventJoin);
  yield takeLatest(TypeActions.CREATE_EVENT_JOIN_REQUEST, createEventJoin);
  yield takeLatest(TypeActions.UPDATE_EVENT_JOIN_REQUEST, updateEventJoin);
  yield takeLatest(TypeActions.DELETE_EVENT_JOIN_REQUEST, deleteEventJoin);
  yield takeLatest(TypeActions.JOIN_EVENT_REQUEST, joinEvent);
}
