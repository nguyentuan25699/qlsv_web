import _ from "lodash";
import { useSelector } from "react-redux";
import { call, put, takeLatest } from "redux-saga/effects";
import TypeActions from "Redux/TypeActions";
import ServiceURL from "Service/ServiceURL";
import { DELETE, GET, PATCH, POST } from "../../Service/ServiceBase";

export function* getUsers(data) {
  const url = ServiceURL.User + "/" + data.params;
  // const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_USERS_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_USERS_SUCCESS,
        data: res.data,
      });
      // callback && callback.success(res.data);
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_USERS_FAILED, error });
    // callback && callback.failed(error.response.data.message);
  }
}

export function* deleteUsers(data) {
  const url = ServiceURL.User + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(DELETE, url);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.DELETE_USERS_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.error.response.data.message);
    } else {
      yield put({
        type: TypeActions.DELETE_USERS_SUCCESS,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.DELETE_USERS_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* getListUsers(data) {
  // console.log({ data });
  const url = ServiceURL.User + "?" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_USERS_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.response);
    } else {
      yield put({
        type: TypeActions.GET_LIST_USERS_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success(res.data);
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_LIST_USERS_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* createUsers(data) {
  const url = ServiceURL.User;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.CREATE_USERS_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.error.response.data.message);
    } else {
      yield put({
        type: TypeActions.CREATE_USERS_SUCCESS,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.CREATE_USERS_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* editUsers(data) {
  const url = ServiceURL.User + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(PATCH, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.EDIT_USERS_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.error.response.data.message);
    } else {
      yield put({
        type: TypeActions.EDIT_USERS_SUCCESS,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.EDIT_USERS_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* logIn(data) {
  const url = ServiceURL.Login;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({ type: TypeActions.LOGIN_REQUEST_FAILED, error: res.message });
      callback && callback.failed(res.message);
    } else {
      yield put({
        type: TypeActions.LOGIN_REQUEST_SUCCESS,
        data: res.data.user,
      });
      localStorage.setItem("expiresAt", res.data.tokens.access.expires);
      localStorage.setItem("token", res.data.tokens.access.token);
      localStorage.setItem("refreshtoken", res.data.tokens.refresh.token);
      localStorage.setItem("role", res.data.user.role);
      localStorage.setItem("id", res.data.user.id);
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.LOGIN_REQUEST_FAILED,
      error: error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export function* logOut(data) {
  const url = "auth/logout";
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({ type: TypeActions.LOG_OUT_FAILED, error: res.message });
      callback && callback.failed(res.message);
    } else {
      yield put({
        type: TypeActions.LOG_OUT_SUCCESS,
        // data: res.data,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.LOG_OUT_FAILED,
      error: error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export function* changePassword(data) {
  const url = ServiceURL.ChangePass + "?" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.CHANGE_PASSWORD_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.message);
    } else {
      yield put({
        type: TypeActions.CHANGE_PASSWORD_SUCCESS,
        // data: res.data,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.CHANGE_PASSWORD_FAILED,
      error: error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export function* forgotPassword(data) {
  const url = ServiceURL.ForgotPass;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.FORGOT_PASSWORD_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.message);
    } else {
      yield put({
        type: TypeActions.FORGOT_PASSWORD_SUCCESS,
        // data: res.data,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.FORGOT_PASSWORD_FAILED,
      error: error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export function* resetPassword(data) {
  const url = ServiceURL.ResetPass + "?" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.RESET_PASSWORD_FAILED,
        error: res.message,
      });
      callback && callback.failed(res.message);
    } else {
      yield put({
        type: TypeActions.RESET_PASSWORD_SUCCESS,
        // data: res.data,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.RESET_PASSWORD_FAILED,
      error: error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export default function* userSaga() {
  yield takeLatest(TypeActions.GET_USERS_REQUEST, getUsers);
  yield takeLatest(TypeActions.DELETE_USERS_REQUEST, deleteUsers);
  yield takeLatest(TypeActions.LOGIN_REQUEST, logIn);
  yield takeLatest(TypeActions.GET_LIST_USERS_REQUEST, getListUsers);
  yield takeLatest(TypeActions.CREATE_USERS_REQUEST, createUsers);
  yield takeLatest(TypeActions.EDIT_USERS_REQUEST, editUsers);
  yield takeLatest(TypeActions.LOG_OUT, logOut);
  yield takeLatest(TypeActions.CHANGE_PASSWORD_REQUEST, changePassword);
  yield takeLatest(TypeActions.FORGOT_PASSWORD_REQUEST, forgotPassword);
  yield takeLatest(TypeActions.RESET_PASSWORD_REQUEST, resetPassword);
}
