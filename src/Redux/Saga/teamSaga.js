import _ from "lodash";
import { call, put, takeLatest } from "redux-saga/effects";
import TypeActions from "Redux/TypeActions";
import ServiceURL from "Service/ServiceURL";
import { DELETE, GET, PATCH, POST } from "../../Service/ServiceBase";

export function* getListTeam(data) {
  const url = ServiceURL.Team + "?" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_TEAM_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_TEAM_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success(res.data);
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_LIST_TEAM_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}
export function* getListTeamShow(data) {
  const url = ServiceURL.Team;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_TEAM_SHOW_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_TEAM_SHOW_SUCCESS,
        // data: res.data.results,
      });
      callback && callback.success(res.data.results);
    }
  } catch (error) {
    yield put({ type: TypeActions.GET_LIST_TEAM_SHOW_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* createTeam(data) {
  const url = ServiceURL.Team;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.CREATE_TEAM_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.CREATE_TEAM_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.CREATE_TEAM_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* deleteTeam(data) {
  const url = ServiceURL.Team + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(DELETE, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.DELETE_TEAM_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.DELETE_TEAM_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.DELETE_TEAM_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export function* updateTeam(data) {
  const url = ServiceURL.Team + "/" + data.params;
  const callback = data.callback;
  try {
    const res = yield call(PATCH, url, data.body);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.UPDATE_TEAM_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.UPDATE_TEAM_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({ type: TypeActions.UPDATE_TEAM_FAILED, error });
    callback && callback.failed(error.response.data.message);
  }
}

export default function* teamSaga() {
  yield takeLatest(TypeActions.GET_LIST_TEAM_REQUEST, getListTeam);
  yield takeLatest(TypeActions.GET_LIST_TEAM_SHOW_REQUEST, getListTeamShow);
  yield takeLatest(TypeActions.CREATE_TEAM_REQUEST, createTeam);
  yield takeLatest(TypeActions.UPDATE_TEAM_REQUEST, updateTeam);
  yield takeLatest(TypeActions.DELETE_TEAM_REQUEST, deleteTeam);
}
