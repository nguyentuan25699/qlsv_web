import { all, fork } from "redux-saga/effects";
import productSaga from "./prodcutSaga";
import userSaga from "./userSaga";
import doorSaga from "./doorSaga";
import eventSaga from "./eventSaga";
import uploadSaga from "./uploadSaga";
import teamSaga from "./teamSaga";
import eventJoinSaga from "./eventJoinSaga";

export function* rootSagas() {
  yield all([
    fork(userSaga),
    fork(eventSaga),
    fork(productSaga),
    fork(doorSaga),
    fork(uploadSaga),
    fork(teamSaga),
    fork(eventJoinSaga),
  ]);
}
export default rootSagas;
