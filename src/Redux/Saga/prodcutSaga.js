import { call, put, takeLatest } from "redux-saga/effects";
import _ from "lodash";
import TypeActions from "Redux/TypeActions";
import { GET } from "Service/ServiceBase";
import ServiceURL from "Service/ServiceURL";
import { POST } from "Service/ServiceBase";
import { DELETE } from "Service/ServiceBase";
import { PATCH } from "Service/ServiceBase";

export function* getListProduct(data) {
  const url = ServiceURL.Product + "?" + data.callback.detail;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_PRODUCT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_PRODUCT_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success(res.data.results, res.data);
    }
  } catch (error) {
    yield put({
      type: TypeActions.GET_LIST_PRODUCT_FAILED,
      error,
    });
    callback && callback.failed(error);
  }
}

//! Get list product by User
export function* getListProductByUser(data) {
  const url = ServiceURL.ProductByUser + "?" + data.callback.detail;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_PRODUCT_BY_USER_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_PRODUCT_BY_USER_SUCCESS,
        data: res.data.results,
      });
      callback && callback.success(res.data.results, res.data);
    }
  } catch (error) {
    yield put({
      type: TypeActions.GET_LIST_PRODUCT_BY_USER_FAILED,
      error,
    });
    callback && callback.failed(error);
  }
}

//!Get list type product
export function* getListTypeProduct(data) {
  const url = ServiceURL.ProductType;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_TYPE_PRODUCT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_TYPE_PRODUCT_SUCCESS,
        data: res.data.productType,
      });
      // callback && callback.success(res.data.productType);
    }
  } catch (error) {
    yield put({
      type: TypeActions.GET_LIST_TYPE_PRODUCT_FAILED,
      error,
    });
  }
}
export function* getListUnitProduct(data) {
  const url = ServiceURL.Unit;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_UNIT_PRODUCT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_UNIT_PRODUCT_SUCCESS,
        data: res.data.productUnit,
      });
      // callback && callback.success(res.data.productUnit);
    }
  } catch (error) {
    yield put({
      type: TypeActions.GET_LIST_UNIT_PRODUCT_FAILED,
      error,
    });
  }
}
export function* getListLevelProduct(data) {
  const url = ServiceURL.Level;
  const callback = data.callback;
  try {
    const res = yield call(GET, url);
    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_LIST_LEVEL_PRODUCT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_LIST_LEVEL_PRODUCT_SUCCESS,
        data: res.data.levels,
      });
      // callback && callback.success(res.data.levels);
    }
  } catch (error) {
    yield put({
      type: TypeActions.GET_LIST_LEVEL_PRODUCT_FAILED,
      error,
    });
  }
}

export function* createProduct(data) {
  const url = ServiceURL.Product;
  const callback = data.callback;
  try {
    const res = yield call(POST, url, data.body);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.CREATE_PRODUCT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.CREATE_PRODUCT_SUCCESS,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.CREATE_PRODUCT_FAILED,
      error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export function* getProduct(data) {
  const callback = data.callback;
  const url = ServiceURL.Product + "/" + callback.idEdit;
  try {
    const res = yield call(GET, url);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.GET_PRODUCT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.GET_PRODUCT_SUCCESS,
        data: res.data,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.GET_PRODUCT_FAILED,
      error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export function* deleteProduct(data) {
  const callback = data.callback;
  const url = ServiceURL.Product + "/" + callback.id;
  try {
    const res = yield call(DELETE, url);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.DELETE_PRODUCT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.DELETE_PRODUCT_SUCCESS,
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.DELETE_PRODUCT_FAILED,
      error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export function* updateProduct(data) {
  const callback = data.callback;

  const url = ServiceURL.Product + "//" + callback.idEdit;
  try {
    const res = yield call(PATCH, url, data.body);

    if (res.message && !_.isEmpty(res.message)) {
      yield put({
        type: TypeActions.UPDATE_PRODUCT_FAILED,
        error: res.message,
      });
    } else {
      yield put({
        type: TypeActions.UPDATE_PRODUCT_SUCCESS,
        // data:
      });
      callback && callback.success();
    }
  } catch (error) {
    yield put({
      type: TypeActions.UPDATE_PRODUCT_FAILED,
      error,
    });
    callback && callback.failed(error.response.data.message);
  }
}

export default function* productSaga() {
  yield takeLatest(TypeActions.GET_LIST_PRODUCT_REQUEST, getListProduct);
  yield takeLatest(
    TypeActions.GET_LIST_TYPE_PRODUCT_REQUEST,
    getListTypeProduct
  );
  yield takeLatest(
    TypeActions.GET_LIST_UNIT_PRODUCT_REQUEST,
    getListUnitProduct
  );
  yield takeLatest(
    TypeActions.GET_LIST_LEVEL_PRODUCT_REQUEST,
    getListLevelProduct
  );
  yield takeLatest(TypeActions.CREATE_PRODUCT_REQUEST, createProduct);
  yield takeLatest(TypeActions.GET_PRODUCT_REQUEST, getProduct);
  yield takeLatest(TypeActions.DELETE_PRODUCT_REQUEST, deleteProduct);
  yield takeLatest(TypeActions.UPDATE_PRODUCT_REQUEST, updateProduct);
  yield takeLatest(
    TypeActions.GET_LIST_PRODUCT_BY_USER_REQUEST,
    getListProductByUser
  );
}
