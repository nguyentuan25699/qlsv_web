import { InputAdornment, makeStyles } from "@material-ui/core";
//Icon
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import AddIcon from "@material-ui/icons/Add";

import { container } from "assets/jss/material-dashboard-react.js";
import CustomInput from "components/CustomInput/CustomInput";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import React, { useState, useRef } from "react";
import Button from "components/CustomButtons/Button";

const style = {
  container,
  left: {
    padding: "15px 0 0 0",
    float: "left!important",
    display: "block",
    width: "100%",
  },
  right: {
    padding: "15px 0",
    margin: "0",
    fontSize: "14px",
    float: "right!important",
    // marginBottom: "0px",
    marginTop: "20px",
    marginBottom: "0px",
  },
};
const useStyle = makeStyles(style);

function ScoreManageSearchBox(props) {
  //!Const
  const { searchChange } = props;
  const typingTimeOutRef = useRef(null);
  const classes = useStyle();
  //!UseState
  const [search, setsearch] = useState("");
  //!function
  const handleSearchChange = (params) => {
    setsearch(params.target.value);
    const value = params.target.value;
    if (!searchChange) return;
    if (typingTimeOutRef.current) {
      clearTimeout(typingTimeOutRef.current);
    }
    typingTimeOutRef.current = setTimeout(() => {
      const searchValue = value;
      searchChange(searchValue);
    }, 300);
  };
  return (
    <div className={classes.container}>
      <GridContainer>
        <GridItem xs={12} sm={12} md={3}>
          <div className={classes.container} style={{ width: "100%" }}>
            <div className={classes.left}>
              <CustomInput
                id="search"
                labelText="Tìm kiếm"
                value={search}
                style={{ color: "white" }}
                onChange={handleSearchChange}
                formControlProps={{
                  fullWidth: true,
                }}
                inputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      {search === "" ? (
                        <SearchIcon
                          fontSize="small"
                          style={{ color: "white", padding: "3px" }}
                        />
                      ) : (
                        <Button
                          color="transparent"
                          size="lg"
                          style={{ padding: "2px" }}
                          onClick={() => {
                            setsearch("");
                            searchChange("");
                          }}
                        >
                          <CloseIcon size="sm" style={{ color: "white" }} />
                        </Button>
                      )}
                    </InputAdornment>
                  ),
                  //   autoComplete: "on",
                }}
              />
            </div>
          </div>
        </GridItem>
        {/* <GridItem xs={12} sm={12} md={9}>
          <div className={classes.right}>
            <Button round size="md" color="transparent">
              <AddIcon />
              Thêm sự kiện
            </Button>
          </div>
        </GridItem> */}
      </GridContainer>
    </div>
  );
}

export default ScoreManageSearchBox;
