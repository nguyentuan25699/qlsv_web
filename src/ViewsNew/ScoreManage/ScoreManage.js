// @material-ui/core components
import { makeStyles, Slide, TablePagination } from "@material-ui/core";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import Popup from "components/Popup/Popup";
import Table from "components/Table/Table.js";
import queryString from "query-string";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "Redux/Actions";
import ScoreDetail from "./ScoreDetail";
import ScoreManageSearchBox from "./ScoreManageSearchBox";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};
const useStyles = makeStyles(styles);
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

function ScoreManage(props) {
  //!Const
  const classes = useStyles();
  const listUser = useSelector((state) => state.userReducer.listUser);
  const dispatch = useDispatch();
  //!State
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [totalResult, settotalResult] = React.useState(0);
  const [openPopup, setopenPopup] = React.useState(false);
  const [isDone, setisDone] = React.useState(false);
  const [scoreDetail, setscoreDetail] = React.useState({
    id: "",
    name: "",
    studentCode: "",
    totalScore: "",
  });
  const [detailUserParams, setdetailUserParams] = React.useState({
    limit: rowsPerPage,
    page: page + 1,
  });
  const [tableScore, settableScore] = React.useState([
    {
      id: "",
      name: "",
      studentCode: "",
      totalScore: "",
    },
  ]);
  //!Useeffect
  // React.useEffect(() => {
  //   dispatch(
  //     userActions.getListUsers(queryString.stringify(detailUserParams), {
  //       success: (data) => {
  //         setPage(data.page - 1);
  //         settotalResult(data.totalResults);
  //       },
  //       failed: (mess) => {},
  //     })
  //   );
  // }, []);
  React.useEffect(() => {
    dispatch(
      userActions.getListUsers(queryString.stringify(detailUserParams), {
        success: (data) => {
          setPage(data.page - 1);
          settotalResult(data.totalResults);
        },
        failed: (mess) => {},
      })
    );
  }, [detailUserParams]);
  React.useEffect(() => {
    setdetailUserParams({
      ...detailUserParams,
      page: page + 1,
      limit: rowsPerPage,
    });
    // window.scrollTo(0, 0);
  }, [page, rowsPerPage]);
  React.useEffect(() => {
    if (isDone) {
      dispatch(
        userActions.getListUsers(queryString.stringify(detailUserParams), {
          success: (data) => {
            setPage(data.page - 1);
            settotalResult(data.totalResults);
          },
          failed: (mess) => {},
        })
      );
      setisDone(false);
    }
  }, [isDone]);
  React.useEffect(() => {
    settableScore(listUser);
  }, [listUser]);
  //!Function
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const roundButtonsFunction = (params) => {
    const roundButtons = [
      { color: "success", icon: VisibilityIcon, detail: params },
      //   { color: "danger", icon: Close, detail: params },
    ].map((btn, key) => {
      return (
        <Button
          round
          justIcon
          size="sm"
          color={btn.color}
          key={key}
          onClick={() => {
            setscoreDetail({
              ...scoreDetail,
              id: btn.detail.id,
              name: btn.detail.name,
              studentCode: btn.detail.studentCode,
              totalScore: btn.detail.totalScore,
            });
            setopenPopup(true);
          }}
        >
          <btn.icon />
        </Button>
      );
    });
    return roundButtons;
  };
  const table = tableScore.map((index) => {
    return [
      index.name,
      index.studentCode,
      index.class + " - K" + index.schoolYear,
      index.totalScore,
      roundButtonsFunction(index),
    ];
  });
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Card>
          <CardHeader color="primary">
            <div
              className={classes.container}
              style={{
                alignItems: "center",
                marginTop: "-25px",
              }}
            >
              <ScoreManageSearchBox
                searchChange={(search) => {
                  if (search === "") {
                    setdetailUserParams({
                      limit: rowsPerPage,
                      page: 1,
                    });
                  } else {
                    // const detailSearch = queryString.stringify({
                    //   name: search,
                    // });
                    // dispatch(userActions.getListUsers(detailSearch));
                    setdetailUserParams({
                      ...detailUserParams,
                      name: search,
                      page: 1,
                    });
                  }
                }}
              />
            </div>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={[
                "Tên",
                "Mã sinh viên",
                "Lớp - Khoá",
                "Tổng điểm",
                "Hành động",
              ]}
              tableData={table}
            />
            <TablePagination
              component="div"
              count={totalResult}
              page={page}
              onChangePage={handleChangePage}
              rowsPerPage={rowsPerPage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </CardBody>
        </Card>
      </GridItem>
      <Popup openPopup={openPopup} title="Chi tiết điểm">
        <ScoreDetail
          detail={scoreDetail}
          setopenPopup={setopenPopup}
          setisDone={setisDone}
        />
      </Popup>
    </GridContainer>
  );
}

export default ScoreManage;
