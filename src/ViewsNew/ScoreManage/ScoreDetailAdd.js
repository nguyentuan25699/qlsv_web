import { FormControl, makeStyles, TextField } from "@material-ui/core";
import { container } from "assets/jss/material-dashboard-react.js";
import customSelectStyle from "assets/jss/material-dashboard-react/customSelectStyle.js";
import Button from "components/CustomButtons/Button";
import CustomInput from "components/CustomInput/CustomInput";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import { Formik } from "formik";
import React from "react";
import { teamActions } from "Redux/Actions";
import Autocomplete from "@material-ui/lab/Autocomplete";
import * as yup from "yup";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch, useSelector } from "react-redux";
import moment from "moment";
import { eventActions } from "Redux/Actions";
import queryString from "query-string";
import { eventJoinActions } from "Redux/Actions";
import { userActions } from "Redux/Actions";

const styles = {
  container,
  ...customSelectStyle,
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};
const useStyles = makeStyles(styles);
function ScoreDetailAdd(props) {
  //!Const
  const eventJoinSchema = yup.object({
    eventId: yup.string().required("Vui lòng chọn sự kiện!"),
    timeRegister: yup.string().required("Vui lòng chọn ngày đăng kí!"),
    timeJoin: yup.string().required("Vui lòng chọn ngày tham gia!"),
  });
  const listEvent = useSelector((state) => state.eventReducer.listEvent);
  const classes = useStyles();
  const dispatch = useDispatch();
  const { setopenPopup, detail, setisDone, setisDone1 } = props;
  //!State
  const [Event, setEvent] = React.useState([]);
  const [searchEvent, setsearchEvent] = React.useState({
    name: "",
  });
  const [DetailScore, setDetailScore] = React.useState({
    eventId: "",
    timeJoin: "",
    timeRegister: "",
  });
  //!Useeffect
  React.useEffect(() => {
    if (searchEvent.name !== "") {
      const params = queryString.stringify(searchEvent);
      dispatch(eventActions.getListEvent(params));
    } else {
      setEvent([]);
    }
  }, [searchEvent]);
  React.useEffect(() => {
    if (!!listEvent && listEvent !== null) {
      setEvent(listEvent);
    }
  }, [listEvent]);
  //!Function
  const handleSubmit = (value) => {
    const body = {
      userId: detail,
      eventId: value.eventId,
      timeJoin: value.timeJoin,
      timeRegister: value.timeRegister,
      status: "Đã Tham gia",
    };
    dispatch(
      eventJoinActions.createEventJoin(body, {
        success: () => {
          setopenPopup(false);
          // const detailget = {
          //   userId: detail.id,
          //   populate: "eventId",
          //   status: "Đã Tham gia",
          // };
          // dispatch(
          //   eventJoinActions.getListEventJoin(queryString.stringify(detailget))
          // );
          setisDone(true);
          setisDone1(true);
          // dispatch(userActions.getListUsers(""));
          store.addNotification({
            title: "Thông báo!",
            message: "Thêm chi tiết sự kiện thành công!",
            type: "success", // 'default', 'success', 'info', 'warning'
            container: "bottom-right", // where to position the notifications
            animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
            dismiss: {
              pauseOnHover: true,
              onScreen: true,
              duration: 3000,
            },
          });
        },
        failed: (error) => {
          store.addNotification({
            title: "Thông báo!",
            message: "Thêm chi tiết sự kiện thất bại! Lỗi: " + error + "!",
            type: "warning", // 'default', 'success', 'info', 'warning'
            container: "bottom-right", // where to position the notifications
            animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
            animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
            dismiss: {
              onScreen: true,
              pauseOnHover: true,
              duration: 5000,
            },
          });
        },
      })
    );
  };
  //!Render
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Formik
          initialValues={DetailScore}
          enableReinitialize
          validationSchema={eventJoinSchema}
          onSubmit={handleSubmit}
        >
          {({ values, setFieldValue, handleSubmit, errors, touched }) => {
            return (
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <Autocomplete
                    style={{ marginTop: "-20px" }}
                    options={Event}
                    // value={values.eventId}
                    getOptionLabel={(option) => {
                      return (
                        option.name +
                        " - " +
                        moment(option.startTime).format("DD/MM/YYYY")
                      );
                    }}
                    onChange={(e, value) => {
                      if (value !== null) {
                        setFieldValue("eventId", value.id);
                      } else {
                        setFieldValue("eventId", "");
                      }
                    }}
                    renderInput={(params) => (
                      <TextField
                        {...params}
                        variant="standard"
                        label="Chọn sự kiện"
                        onChange={(e) => {
                          setsearchEvent({
                            ...searchEvent,
                            name: e.target.value,
                          });
                        }}
                        margin="normal"
                        fullWidth
                      />
                    )}
                  />
                  {errors.eventId && touched.eventId && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.eventId}
                    </a>
                  )}
                  <FormControl fullWidth className={classes.selectFormControl}>
                    <TextField
                      id="datetime-local"
                      label="Thời gian đăng kí sự kiện"
                      type="datetime-local"
                      defaultValue={moment(new Date()).format(
                        "YYYY-MM-DDTHH:mm"
                      )}
                      value={
                        !!values.timeRegister && values.timeRegister !== ""
                          ? moment(values.timeRegister).format(
                              "YYYY-MM-DDTHH:mm"
                            )
                          : null
                      }
                      className={classes.textField}
                      onChange={(time) => {
                        setFieldValue(
                          "timeRegister",
                          new Date(time.target.value).toISOString()
                        );
                      }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.timeRegister && touched.timeRegister && (
                      <a
                        style={{
                          fontSize: 12,
                          color: "red",
                        }}
                      >
                        {errors.timeRegister}
                      </a>
                    )}
                  </FormControl>
                  <FormControl fullWidth className={classes.selectFormControl}>
                    <TextField
                      id="datetime-local"
                      label="Thời gian tham gia sự kiện"
                      type="datetime-local"
                      defaultValue={moment(new Date()).format(
                        "YYYY-MM-DDTHH:mm"
                      )}
                      value={
                        !!values.timeJoin && values.timeJoin !== ""
                          ? moment(values.timeJoin).format("YYYY-MM-DDTHH:mm")
                          : null
                      }
                      className={classes.textField}
                      onChange={(time) => {
                        setFieldValue(
                          "timeJoin",
                          new Date(time.target.value).toISOString()
                        );
                      }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.timeJoin && touched.timeJoin && (
                      <a
                        style={{
                          fontSize: 12,
                          color: "red",
                        }}
                      >
                        {errors.timeJoin}
                      </a>
                    )}
                  </FormControl>

                  <div
                    className={classes.container}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "10px",
                    }}
                  >
                    <Button color="success" round onClick={handleSubmit}>
                      Thêm
                    </Button>
                    &nbsp;
                    <Button round onClick={() => setopenPopup(false)}>
                      Huỷ
                    </Button>
                  </div>
                </GridItem>
              </GridContainer>
            );
          }}
        </Formik>
      </GridItem>
    </GridContainer>
  );
}

export default ScoreDetailAdd;
