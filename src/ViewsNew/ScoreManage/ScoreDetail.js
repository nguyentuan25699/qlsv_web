import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  Slide,
  TablePagination,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";
import AddIcon from "@material-ui/icons/Add";
import { container } from "assets/jss/material-dashboard-react.js";
import Button from "components/CustomButtons/Button";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Popup from "components/Popup/Popup";
import Table from "components/Table/Table.js";
import moment from "moment";
import queryString from "query-string";
import React from "react";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "Redux/Actions";
import eventJoinActions from "Redux/Actions/eventJoinActions";
import ScoreDetailAdd from "./ScoreDetailAdd";

const useStyle = makeStyles({
  container,
});
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

function ScoreDetail(props) {
  //!Const
  const dispatch = useDispatch();
  const { detail, setopenPopup, setisDone } = props;
  const classes = useStyle();
  const listEventJoin = useSelector(
    (state) => state.eventJoinReducer.listEventJoin
  );
  //!State
  const [scoreDetail, setscoreDetail] = React.useState([]);
  const [smallModal, setSmallModal] = React.useState(false);
  const [isDone1, setisDone1] = React.useState(false);
  const [openPopup1, setopenPopup1] = React.useState(false);
  const [iddelete, setiddelete] = React.useState("");
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [totalResult, settotalResult] = React.useState(0);
  const [detailListJoin, setdetailListJoin] = React.useState({
    limit: rowsPerPage,
    page: page + 1,
    userId: detail.id,
    populate: "eventId, userId",
    status: "Đã Tham gia",
    sortBy: "timeJoin:desc",
  });

  //!Use effect
  React.useEffect(() => {
    setdetailListJoin({
      ...detailListJoin,
      page: page + 1,
      limit: rowsPerPage,
    });
  }, [page, rowsPerPage]);
  // React.useEffect(() => {
  //   // const detailget = {
  //   //   userId: detail.id,
  //   //   populate: "eventId, userId",
  //   //   status: "Đã Tham gia",
  //   // };
  //   dispatch(
  //     eventJoinActions.getListEventJoin(queryString.stringify(detailListJoin), {
  //       success: (data) => {
  //         setPage(data.page - 1);
  //         settotalResult(data.totalResults);
  //       },
  //       failed: (mess) => {},
  //     })
  //   );
  // }, []);
  React.useEffect(() => {
    // const detailget = {
    //   userId: detail.id,
    //   populate: "eventId, userId",
    //   status: "Đã Tham gia",
    // };
    dispatch(
      eventJoinActions.getListEventJoin(queryString.stringify(detailListJoin), {
        success: (data) => {
          setPage(data.page - 1);
          settotalResult(data.totalResults);
        },
        failed: (mess) => {},
      })
    );
  }, [detailListJoin]);
  React.useEffect(() => {
    if (isDone1) {
      // const detailget = {
      //   userId: detail.id,
      //   populate: "eventId, userId",
      //   status: "Đã Tham gia",
      // };
      dispatch(
        eventJoinActions.getListEventJoin(
          queryString.stringify(detailListJoin),
          {
            success: (data) => {
              setPage(data.page - 1);
              settotalResult(data.totalResults);
            },
            failed: (mess) => {},
          }
        )
      );
      setisDone1(false);
    }
  }, [isDone1]);
  React.useEffect(() => {
    if (!!listEventJoin && listEventJoin !== null) {
      setscoreDetail(listEventJoin);
    }
  }, [listEventJoin]);
  //!Function
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const buttonFunction = (params) => {
    const roundButtons = [{ color: "danger", icon: Close, detail: params }].map(
      (btn, key) => {
        return (
          <Button
            round
            justIcon
            size="sm"
            color={btn.color}
            key={key}
            onClick={() => {
              setSmallModal(true);
              setiddelete(btn.detail.id);
            }}
          >
            <btn.icon />
          </Button>
        );
      }
    );
    return roundButtons;
  };
  //!
  const table = scoreDetail.map((index) => {
    return [
      index.eventId.name,
      moment(index.eventId.startTime).format("hh:mm DD/MM/YYYY"),
      moment(index.timeJoin).format("hh:mm DD/MM/YYYY"),
      index.eventId.score,
      buttonFunction(index),
    ];
  });
  return (
    <GridContainer>
      {/* SMALL MODAL START */}
      <Dialog
        classes={{
          root: classes.modalRoot,
          paper: classes.modal + " " + classes.modalSmall,
        }}
        open={smallModal}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => setSmallModal(false)}
        aria-labelledby="small-modal-slide-title"
        aria-describedby="small-modal-slide-description"
      >
        <DialogTitle
          id="small-modal-slide-title"
          disableTypography
          className={classes.modalHeader}
        >
          <div className={classes.container} style={{ display: "flex" }}>
            <h3 style={{ color: "red" }}>
              <b>Thông báo!</b>
            </h3>
            <Button
              simple
              className={classes.modalCloseButton}
              key="close"
              aria-label="Close"
              onClick={() => setSmallModal(false)}
            >
              <Close className={classes.modalClose} />
            </Button>
          </div>
        </DialogTitle>
        <DialogContent
          id="small-modal-slide-description"
          className={classes.modalBody + " " + classes.modalSmallBody}
        >
          <h5>Bạn có chắc chắn muốn xoá?</h5>
        </DialogContent>
        <DialogActions
          className={classes.modalFooter + " " + classes.modalFooterCenter}
        >
          <Button
            onClick={() => setSmallModal(false)}
            link
            className={classes.modalSmallFooterFirstButton}
          >
            Không
          </Button>
          <Button
            onClick={() => {
              dispatch(
                eventJoinActions.deleteEventJoin(iddelete, {
                  success: () => {
                    // const detailget = {
                    //   userId: detail.id,
                    //   populate: "eventId",
                    //   status: "Đã Tham gia",
                    // };
                    dispatch(
                      eventJoinActions.getListEventJoin(
                        queryString.stringify(detailListJoin),
                        {
                          success: (data) => {
                            setPage(data.page - 1);
                            settotalResult(data.totalResults);
                          },
                          failed: (mess) => {},
                        }
                      )
                    );
                    setisDone(true);
                    // dispatch(userActions.getListUsers(""));
                    store.addNotification({
                      title: "Thông báo",
                      message: "Xoá thành công!",
                      type: "success", // 'default', 'success', 'info', 'warning'
                      container: "bottom-right", // where to position the notifications
                      animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                      animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                      dismiss: {
                        pauseOnHover: true,
                        onScreen: true,
                        duration: 3000,
                      },
                    });
                  },
                  failed: (error) => {
                    store.addNotification({
                      title: "Thông báo",
                      message: "Xoá thất bại! Lỗi: " + error + "!",
                      type: "warning", // 'default', 'success', 'info', 'warning'
                      container: "bottom-right", // where to position the notifications
                      animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                      animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                      dismiss: {
                        pauseOnHover: true,
                        duration: 3000,
                        onScreen: true,
                      },
                    });
                  },
                })
              );
              setSmallModal(false);
            }}
            color="success"
            simple
            className={
              classes.modalSmallFooterFirstButton +
              " " +
              classes.modalSmallFooterSecondButton
            }
          >
            Có
          </Button>
        </DialogActions>
      </Dialog>
      {/* SMALL MODAL END */}

      <GridItem xs={12} sm={12} md={12}>
        <div
          className={classes.container}
          style={{ justifyContent: "space-between" }}
        >
          <div style={{ padding: "0 0 10px 0" }}>
            <b>Họ tên: </b>
            {detail.name}
          </div>
          <div style={{ padding: "0 0 10px 0" }}>
            <b>Mã sinh viên: </b>
            {detail.studentCode}
          </div>
          {/* <div style={{ padding: "0 0 10px 0" }}>
            <b>Tổng điểm: </b>
            {detail.totalScore}
          </div> */}
        </div>
        <hr style={{ color: "gray" }} />
      </GridItem>
      <GridItem xs={12} sm={12} md={12}>
        <div style={{ float: "right" }}>
          <Button round color="primary" onClick={() => setopenPopup1(true)}>
            <AddIcon />
            Thêm chi tiết điểm hoạt động
          </Button>
        </div>
      </GridItem>
      <GridItem xs={12} sm={12} md={12}>
        <div className={classes.container}>
          <Table
            tableHeaderColor="primary"
            tableHead={[
              "Tên sự kiện",
              "Ngày diễn ra",
              "Ngày tham gia",
              "Điểm sự kiện",
              "Hành động",
            ]}
            tableData={table}
          />
          <TablePagination
            component="div"
            count={totalResult}
            page={page}
            onChangePage={handleChangePage}
            rowsPerPage={rowsPerPage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
        </div>
      </GridItem>
      <div className={classes.container} style={{ marginTop: "10px" }}>
        <Button onClick={() => setopenPopup(false)} round>
          Đóng
        </Button>
      </div>
      <Popup openPopup={openPopup1} title="Thêm chi tiết điểm">
        {/* <ScoreDetail detail={scoreDetail} setopenPopup={setopenPopup} /> */}
        <ScoreDetailAdd
          setopenPopup={setopenPopup1}
          detail={detail.id}
          setisDone={setisDone}
          setisDone1={setisDone1}
        />
      </Popup>
    </GridContainer>
  );
}

export default ScoreDetail;
