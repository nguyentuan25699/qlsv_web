import {
  FormControl,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core";
import Autocomplete from "@material-ui/lab/Autocomplete";
import { container } from "assets/jss/material-dashboard-react.js";
import customSelectStyle from "assets/jss/material-dashboard-react/customSelectStyle.js";
import Button from "components/CustomButtons/Button";
import CustomInput from "components/CustomInput/CustomInput";
import ImageUpload from "components/CustomUpload/ImageUpload";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import { Formik } from "formik";
import moment from "moment";
import queryString from "query-string";
import React from "react";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch, useSelector } from "react-redux";
import { teamActions, uploadActions } from "Redux/Actions";
import userActions from "Redux/Actions/userActions";
import { BASE_URL_IMAGE } from "Service/ServiceURL";
import * as yup from "yup";

const styles = {
  container,
  ...customSelectStyle,
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};
const useStyles = makeStyles(styles);
function UserManageAdd(props) {
  //!Const
  const dispatch = useDispatch();
  const listTeam = useSelector((state) => state.teamReducer.listTeam);
  //!State
  const [gender, setgender] = React.useState(["nam", "nữ"]);
  const [role, setrole] = React.useState(["admin", "user"]);
  const [team, setteam] = React.useState([]);
  const [searchTeam, setsearchTeam] = React.useState({ name: "" });
  const [teamDetail, setteamDetail] = React.useState([]);
  const [detailUser, setdetailUser] = React.useState({
    id: "",
    name: "",
    email: "",
    gender: "",
    dateOfBird: "",
    studentCode: "",
    class: "",
    schoolYear: "",
    faculty: "",
    address: "",
    phoneNumber: "",
    role: "",
    image: "",
    teamID: [],
    password: "",
  });

  //!Const
  const { setopenPopup, detail, setisDone } = props;
  const userSchemaAdd = yup.object({
    name: yup.string().required("Vui lòng nhập tên người dùng!"),
    email: yup
      .string()
      .required("Vui lòng nhập email!")
      .email("Email không hợp lệ!"),
    gender: yup.string().required("Vui lòng chọn giới tính!"),
    dateOfBird: yup.string().required("Vui lòng chọn ngày sinh!"),
    studentCode: yup
      .string()
      .required("Vui lòng nhập mã sinh viên!")
      .matches("^\\d+$", "Mã sinh viên không hợp lệ!")
      .min(10, ({ min }) => `Mã sinh viên phải có ${min} ký tự!`)
      .max(10, ({ max }) => `Mã sinh viên phải có ${max} ký tự!`),
    class: yup.string().required("Vui lòng nhập lớp!"),
    schoolYear: yup.string().required("Vui lòng nhập khoá!"),
    faculty: yup.string().required("Vui lòng nhập Khoa!"),
    address: yup.string().required("Vui lòng nhập địa chỉ!"),
    phoneNumber: yup
      .string()
      .required("Vui lòng nhập số điện thoại!")
      .matches(
        "^(0|\\+84)(\\s|\\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\\d)(\\s|\\.)?(\\d{3})(\\s|\\.)?(\\d{3})$",
        "Số điện thoại không hợp lệ!"
      ),
    role: yup.string().required("Vui lòng chọn phân quyền!"),
    password: yup
      .string()
      .required("Vui lòng nhập mật khẩu!")
      .matches(
        "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
        "Mật khẩu phải có ít nhất 8 kí tự, trong đó chứa ít nhất 1 kí tự và một số!"
      ),
  });
  const userSchemaEdit = yup.object({
    name: yup.string().required("Vui lòng nhập tên người dùng!"),
    email: yup
      .string()
      .required("Vui lòng nhập email!")
      .email("Email không hợp lệ!"),
    gender: yup.string().required("Vui lòng chọn giới tính!"),
    dateOfBird: yup.string().required("Vui lòng chọn ngày sinh!"),
    studentCode: yup
      .string()
      .required("Vui lòng nhập mã sinh viên!")
      .matches("^\\d+$", "Mã sinh viên không hợp lệ!")
      .min(10, ({ min }) => `Mã sinh viên phải có ${min} ký tự!`)
      .max(10, ({ max }) => `Mã sinh viên phải có ${max} ký tự!`),
    class: yup.string().required("Vui lòng nhập lớp!"),
    schoolYear: yup.string().required("Vui lòng nhập khoá!"),
    faculty: yup.string().required("Vui lòng nhập Khoa!"),
    address: yup.string().required("Vui lòng nhập địa chỉ!"),
    phoneNumber: yup
      .string()
      .required("Vui lòng nhập số điện thoại!")
      .matches(
        "^(0|\\+84)(\\s|\\.)?((3[2-9])|(5[689])|(7[06-9])|(8[1-689])|(9[0-46-9]))(\\d)(\\s|\\.)?(\\d{3})(\\s|\\.)?(\\d{3})$",
        "Số điện thoại không hợp lệ!"
      ),
    role: yup.string().required("Vui lòng chọn phân quyền!"),
    password: yup
      .string()
      // .required("Vui lòng nhập mật khẩu!")
      .matches(
        "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
        "Mật khẩu phải có ít nhất 8 kí tự, trong đó chứa ít nhất 1 kí tự và một số!"
      ),
  });
  const classes = useStyles();
  //!Useeffect
  React.useEffect(() => {
    if (!!detail && detail !== null) {
      setdetailUser({
        ...detailUser,
        name: detail.name,
        gender: detail.gender,
        dateOfBird: detail.dateOfBird,
        studentCode: detail.studentCode,
        class: detail.class,
        email: detail.email,
        schoolYear: detail.schoolYear,
        faculty: detail.faculty,
        address: detail.address,
        phoneNumber: detail.phoneNumber,
        role: detail.role,
        teamID: detail.teamID,
        // teamID: [],
        image: detail.image,
        password: detail.password,
      });
      dispatch(
        teamActions.getListTeamShow({
          success: (data) => {
            const listOwnTeams = [];
            data.map((index) => {
              detail.teamID.map((index1) => {
                if (index.id === index1) {
                  listOwnTeams.push(index);
                }
              });
            });
            setteamDetail(listOwnTeams);
          },
          failed: (mess) => {},
        })
      );
    }
  }, [detail]);
  React.useEffect(() => {
    if (searchTeam.name !== "") {
      const params = queryString.stringify(searchTeam);
      dispatch(teamActions.getListTeam(params));
      //   setstudents(listUser);
    } else {
      setteam([]);
    }
  }, [searchTeam]);
  React.useEffect(() => {
    if (!!listTeam && listTeam !== "") {
      setteam(listTeam);
    }
  }, [listTeam]);
  //!Function
  const handleSubmit = (value) => {
    // const teamID = value.teamID.map((index) => {
    //   return index.id;
    // });
    const body = {
      name: value.name,
      email: value.email,
      gender: value.gender,
      dateOfBird: value.dateOfBird,
      studentCode: value.studentCode,
      class: value.class,
      schoolYear: value.schoolYear,
      faculty: value.faculty,
      address: value.address,
      phoneNumber: value.phoneNumber,
      role: value.role,
      teamID: value.teamID,
      image: value.image,
      password: value.password,
    };
    if (!!detail && detail !== null) {
      dispatch(
        userActions.editUsers(body, detail.id, {
          success: () => {
            setopenPopup(false);
            setisDone(true);
            // dispatch(userActions.getListUsers(""));
            store.addNotification({
              title: "Thông báo!",
              message: "Cập nhật khoản thành công!",
              type: "success", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                pauseOnHover: true,
                onScreen: true,
                duration: 3000,
              },
            });
            if (localStorage.getItem("id") === detail.id) {
              window.location.reload(true);
            }
          },
          failed: (error) => {
            // dispatch(userActions.getListUsers(""));
            store.addNotification({
              title: "Thông báo!",
              message: "Cập nhật khoản thất bại! Lỗi: " + error + "!",
              type: "warning", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                onScreen: true,
                pauseOnHover: true,
                duration: 5000,
              },
            });
          },
        })
      );
    } else {
      dispatch(
        userActions.createUsers(body, {
          success: () => {
            setisDone(true);
            setopenPopup(false);
            // dispatch(userActions.getListUsers(""));
            store.addNotification({
              title: "Thông báo!",
              message: "Tạo khoản thành công!",
              type: "success", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                pauseOnHover: true,
                onScreen: true,
                duration: 3000,
              },
            });
          },
          failed: (error) => {
            // dispatch(userActions.getListUsers(""));
            store.addNotification({
              title: "Thông báo!",
              message: "Tạo khoản thất bại! Lỗi: " + error + "!",
              type: "warning", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                onScreen: true,
                pauseOnHover: true,
                duration: 5000,
              },
            });
          },
        })
      );
    }
  };
  //!Render
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Formik
          validationSchema={
            !!detail && detail !== null ? userSchemaEdit : userSchemaAdd
          }
          initialValues={detailUser}
          enableReinitialize
          onSubmit={handleSubmit}
        >
          {({ values, setFieldValue, handleSubmit, errors, touched }) => {
            return (
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <div
                    className={classes.container}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      width: "50%",
                      height: "auto",
                    }}
                  >
                    <ImageUpload
                      avatar={
                        values.image !== "" ? BASE_URL_IMAGE + values.image : ""
                      }
                      files={(file) => {
                        const fd = new FormData();
                        fd.append("image", file, file.name);
                        dispatch(
                          uploadActions.uploadImage(fd, {
                            success: (img) => {
                              setFieldValue("image", img);
                              store.addNotification({
                                title: "Thông báo!",
                                message: "Chọn và tải ảnh lên thành công!",
                                type: "success", // 'default', 'success', 'info', 'warning'
                                container: "bottom-right", // where to position the notifications
                                animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                                animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                                dismiss: {
                                  pauseOnHover: true,
                                  onScreen: true,
                                  duration: 3000,
                                },
                              });
                            },
                            failed: (mess) => {
                              store.addNotification({
                                title: "Thông báo!",
                                message:
                                  "Chọn và tải ảnh lên thất bại! Lỗi: " +
                                  mess +
                                  "!",
                                type: "warning", // 'default', 'success', 'info', 'warning'
                                container: "bottom-right", // where to position the notifications
                                animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                                animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                                dismiss: {
                                  onScreen: true,
                                  pauseOnHover: true,
                                  duration: 5000,
                                },
                              });
                            },
                          })
                        );
                      }}
                    />
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    id="name"
                    labelText="Họ tên"
                    value={values.name}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("name", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.name && touched.name && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.name}
                    </a>
                  )}
                  <FormControl
                    fullWidth
                    className={classes.selectFormControl}
                    style={{ minWidth: "200px" }}
                  >
                    <InputLabel
                      htmlFor="simple-select"
                      className={classes.selectLabel}
                    >
                      Giới tính
                    </InputLabel>

                    <Select
                      MenuProps={{
                        className: classes.selectMenu,
                      }}
                      classes={{
                        select: classes.select,
                      }}
                      value={values.gender}
                      onChange={(text) =>
                        setFieldValue("gender", text.target.value)
                      }
                      inputProps={{
                        name: "simpleSelect",
                        id: "simple-select",
                      }}
                    >
                      {gender.map((cate, key) => {
                        return (
                          <MenuItem
                            key={key}
                            classes={{
                              root: classes.selectMenuItem,
                              selected: classes.selectMenuItemSelected,
                            }}
                            value={cate}
                          >
                            {cate}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                  {errors.gender && touched.gender && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.gender}
                    </a>
                  )}
                  <FormControl fullWidth className={classes.selectFormControl}>
                    <TextField
                      id="date"
                      label="Ngày sinh"
                      type="date"
                      defaultValue={moment(new Date()).format("YYYY-MM-DD")}
                      value={
                        values.dateOfBird !== ""
                          ? moment(values.dateOfBird).format("YYYY-MM-DD")
                          : null
                      }
                      className={classes.textField}
                      InputLabelProps={
                        {
                          //   shrink: true,
                        }
                      }
                      onChange={(time) => {
                        setFieldValue(
                          "dateOfBird",
                          new Date(time.target.value).toISOString()
                        );
                      }}
                    />
                    {/* <TextField
                    id="datetime-local"
                    label="Thowif gian"
                    type="datetime-local"
                    // defaultValue="2017-05-24T10:30"
                    value={moment(new Date()).format("YYYY-MM-DDTHH:mm")}
                    // value="2017-05-24T10:30"
                    className={classes.textField}
                    onChange={(time) => {
                      console.log("time", new Date(time.target.value));
                    }}
                    InputLabelProps={{
                      shrink: true,
                    }}
                  /> */}
                  </FormControl>
                  {errors.dateOfBird && touched.dateOfBird && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.dateOfBird}
                    </a>
                  )}
                  <CustomInput
                    id="address"
                    labelText="Địa chỉ"
                    value={values.address}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("address", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      //   autoComplete: "on",
                      autoComplete: "off",
                      type: "text",
                    }}
                  />
                  {errors.address && touched.address && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.address}
                    </a>
                  )}
                  <CustomInput
                    id="studentcode"
                    labelText="Mã sinh viên"
                    value={values.studentCode}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("studentCode", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.studentCode && touched.studentCode && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.studentCode}
                    </a>
                  )}
                  <FormControl
                    fullWidth
                    className={classes.selectFormControl}
                    style={{ minWidth: "200px" }}
                  >
                    <InputLabel
                      htmlFor="simple-select"
                      className={classes.selectLabel}
                    >
                      Phân quyền
                    </InputLabel>

                    <Select
                      MenuProps={{
                        className: classes.selectMenu,
                      }}
                      classes={{
                        select: classes.select,
                      }}
                      value={values.role}
                      onChange={(text) =>
                        setFieldValue("role", text.target.value)
                      }
                      inputProps={{
                        name: "simpleSelect",
                        id: "simple-select",
                      }}
                    >
                      {role.map((cate, key) => {
                        return (
                          <MenuItem
                            key={key}
                            classes={{
                              root: classes.selectMenuItem,
                              selected: classes.selectMenuItemSelected,
                            }}
                            value={cate}
                          >
                            {cate}
                          </MenuItem>
                        );
                      })}
                    </Select>
                  </FormControl>
                  {errors.role && touched.role && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.role}
                    </a>
                  )}
                  <FormControl
                    fullWidth
                    className={classes.selectFormControl}
                    style={{ minWidth: "200px" }}
                  >
                    <Autocomplete
                      multiple
                      style={{ marginTop: "-17px" }}
                      options={team}
                      value={teamDetail}
                      getOptionLabel={(option) => {
                        return option.code + " - " + option.name;
                      }}
                      onChange={(e, value) => {
                        if (value !== null) {
                          // const id = value.map((index) => {
                          //   return index.id;
                          // });
                          const list = value.map((index) => {
                            return index.id;
                          });
                          setFieldValue("teamID", list);
                          setteamDetail(value);
                        } else {
                          setFieldValue("teamID", []);
                          setteamDetail([]);
                        }
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          variant="standard"
                          label="Chọn Đội - Nhóm"
                          onChange={(e) => {
                            setsearchTeam({
                              ...searchTeam,
                              name: e.target.value,
                            });
                          }}
                          margin="normal"
                          fullWidth
                        />
                      )}
                    />
                  </FormControl>
                </GridItem>
                <GridItem xs={12} sm={12} md={4}>
                  <CustomInput
                    id="class"
                    labelText="Lớp"
                    value={values.class}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("class", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.class && touched.class && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.class}
                    </a>
                  )}
                  <CustomInput
                    id="schoolYear"
                    labelText="Khoá"
                    value={values.schoolYear}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("schoolYear", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.schoolYear && touched.schoolYear && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.schoolYear}
                    </a>
                  )}
                  <CustomInput
                    id="faculty"
                    labelText="Khoa"
                    value={values.faculty}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("faculty", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.faculty && touched.faculty && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.faculty}
                    </a>
                  )}
                  <CustomInput
                    id="phoneNumber"
                    labelText="Số điện thoại"
                    value={values.phoneNumber}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("phoneNumber", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.phoneNumber && touched.phoneNumber && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.phoneNumber}
                    </a>
                  )}
                  <CustomInput
                    id="pass"
                    disabled={!!props.isPersonal && props.isPersonal}
                    labelText="Mật khẩu"
                    value={values.password}
                    onChange={(text) => {
                      setFieldValue("password", text.target.value);
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      type: "password",
                      autoComplete: "off",
                    }}
                  />
                  {errors.password && touched.password && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.password}
                    </a>
                  )}
                  <CustomInput
                    id="email"
                    labelText="Email"
                    value={values.email}
                    onChange={(text) => {
                      setFieldValue("email", text.target.value);
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      type: "email",
                      autoComplete: "off",
                    }}
                  />
                  {errors.email && touched.email && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.email}
                    </a>
                  )}
                  <div
                    className={classes.container}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "15px",
                    }}
                  >
                    <Button color="success" round onClick={handleSubmit}>
                      {!!detail && detail !== null ? "Cập nhật" : "Thêm"}
                    </Button>
                    &nbsp;
                    <Button round onClick={() => setopenPopup(false)}>
                      Huỷ
                    </Button>
                  </div>
                </GridItem>
              </GridContainer>
            );
          }}
        </Formik>
      </GridItem>
    </GridContainer>
  );
}

export default UserManageAdd;
