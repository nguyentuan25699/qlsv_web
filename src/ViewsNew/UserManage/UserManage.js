// @material-ui/core components
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  Slide,
  TablePagination,
} from "@material-ui/core";
import { Close, Edit } from "@material-ui/icons";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import Popup from "components/Popup/Popup";
import Table from "components/Table/Table.js";
import React from "react";
import UserManageAdd from "./UserManageAdd";
import UserManageSearchBox from "./UserManageSearchBox";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import userActions from "Redux/Actions/userActions";
import queryString from "query-string";
// import ScoreManageSearchBox from "./ScoreManageSearchBox";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};
const useStyles = makeStyles(styles);
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

function UserManage(props) {
  //!Const
  const dispatch = useDispatch();
  const listUser = useSelector((state) => state.userReducer.listUser);
  //!State
  const [openPopup, setopenPopup] = React.useState(false);
  const [smallModal, setSmallModal] = React.useState(false);
  const [iddelete, setiddelete] = React.useState("");
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [totalResult, settotalResult] = React.useState(0);
  const [isDone, setisDone] = React.useState(false);
  const [detailUserParams, setdetailUserParams] = React.useState({
    limit: rowsPerPage,
    page: page + 1,
  });
  const [userDetail, setuserDetail] = React.useState([
    {
      name: "",
      gender: "",
      dateOfBird: "",
      studentCode: "",
      class: "",
      schoolYear: "",
      faculty: "",
      address: "",
      phoneNumber: "",
      role: "",
      image: "",
      teamID: [],
      password: "",
      email: "",
    },
  ]);
  const [detailEdit, setdetailEdit] = React.useState({
    name: "",
    gender: "",
    dateOfBird: "",
    studentCode: "",
    class: "",
    schoolYear: "",
    email: "",
    faculty: "",
    address: "",
    phoneNumber: "",
    role: "",
    teamID: [],
    image: "",
    password: "",
  });
  //!Const
  const classes = useStyles();

  //!Function
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const roundButtonsFunction = (params) => {
    const roundButtons = [
      { color: "success", icon: Edit, detail: params },
      { color: "danger", icon: Close, detail: params },
    ].map((btn, key) => {
      // const toNew = {
      //   pathname: "/main/users-manage/" + btn.detail,
      // };
      return btn.color === "success" ? (
        // <Link to={toNew}>
        <Button
          round
          justIcon
          size="sm"
          color={btn.color}
          key={key}
          onClick={() => {
            setdetailEdit({
              ...detailEdit,
              name: btn.detail.name,
              gender: btn.detail.gender,
              dateOfBird: btn.detail.dateOfBird,
              studentCode: btn.detail.studentCode,
              class: btn.detail.class,
              schoolYear: btn.detail.schoolYear,
              faculty: btn.detail.faculty,
              email: btn.detail.email,
              address: btn.detail.address,
              phoneNumber: btn.detail.phoneNumber,
              role: btn.detail.role,
              image: btn.detail.image,
              password: btn.detail.password,
              teamID: btn.detail.teamID,
              id: btn.detail.id,
            });
            setopenPopup(true);
          }}
        >
          <btn.icon />
        </Button>
      ) : (
        // </Link>
        <Button
          round
          justIcon
          size="sm"
          color={btn.color}
          key={key}
          onClick={() => {
            setSmallModal(true);
            setiddelete(btn.detail.id);
          }}
        >
          <btn.icon />
        </Button>
      );
    });
    return roundButtons;
  };
  //!UseEffect
  React.useEffect(() => {
    setdetailUserParams({
      ...detailUserParams,
      page: page + 1,
      limit: rowsPerPage,
    });
    // window.scrollTo(0, 0);
  }, [page, rowsPerPage]);
  // React.useEffect(() => {
  //   dispatch(
  //     userActions.getListUsers(queryString.stringify(detailUserParams), {
  //       success: (data) => {
  //         setPage(data.page - 1);
  //         settotalResult(data.totalResults);
  //       },
  //       failed: (mess) => {},
  //     })
  //   );
  // }, []);
  React.useEffect(() => {
    if (isDone) {
      dispatch(
        userActions.getListUsers(queryString.stringify(detailUserParams), {
          success: (data) => {
            setPage(data.page - 1);
            settotalResult(data.totalResults);
          },
          failed: (mess) => {},
        })
      );
      setisDone(false);
    }
  }, [isDone]);
  React.useEffect(() => {
    dispatch(
      userActions.getListUsers(queryString.stringify(detailUserParams), {
        success: (data) => {
          setPage(data.page - 1);
          settotalResult(data.totalResults);
        },
        failed: (mess) => {},
      })
    );
  }, [detailUserParams]);
  React.useEffect(() => {
    if (!!listUser && listUser !== null) {
      setuserDetail(listUser);
    }
  }, [listUser]);

  const table = userDetail.map((index) => {
    return [
      index.name,
      index.gender,
      moment(index.dateOfBird).format("DD/MM/YYYY"),
      index.studentCode,
      index.class + " - K" + index.schoolYear,
      index.faculty,
      index.address,
      index.phoneNumber,
      index.email,
      roundButtonsFunction(index),
    ];
  });
  //!Render
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        {/* SMALL MODAL START */}
        <Dialog
          classes={{
            root: classes.modalRoot,
            paper: classes.modal + " " + classes.modalSmall,
          }}
          open={smallModal}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => setSmallModal(false)}
          aria-labelledby="small-modal-slide-title"
          aria-describedby="small-modal-slide-description"
        >
          <DialogTitle
            id="small-modal-slide-title"
            disableTypography
            className={classes.modalHeader}
          >
            <div className={classes.container} style={{ display: "flex" }}>
              <h3 style={{ color: "red" }}>
                <b>Thông báo!</b>
              </h3>
              <Button
                simple
                className={classes.modalCloseButton}
                key="close"
                aria-label="Close"
                onClick={() => setSmallModal(false)}
              >
                <Close className={classes.modalClose} />
              </Button>
            </div>
          </DialogTitle>
          <DialogContent
            id="small-modal-slide-description"
            className={classes.modalBody + " " + classes.modalSmallBody}
          >
            <h5>Bạn có chắc chắn muốn xoá tài khoản?</h5>
          </DialogContent>
          <DialogActions
            className={classes.modalFooter + " " + classes.modalFooterCenter}
          >
            <Button
              onClick={() => setSmallModal(false)}
              link
              className={classes.modalSmallFooterFirstButton}
            >
              Không
            </Button>
            <Button
              onClick={() => {
                // const id = iddelete;
                setSmallModal(false);
                if (iddelete !== localStorage.getItem("id")) {
                  dispatch(
                    userActions.deleteUsers(iddelete, {
                      success: () => {
                        // dispatch(userActions.getListUsers(""));
                        setisDone(true);
                        store.addNotification({
                          title: "Thông báo",
                          message: "Xoá thành công!",
                          type: "success", // 'default', 'success', 'info', 'warning'
                          container: "bottom-right", // where to position the notifications
                          animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                          animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                          dismiss: {
                            pauseOnHover: true,
                            onScreen: true,
                            duration: 3000,
                          },
                        });
                      },
                      failed: (error) => {
                        store.addNotification({
                          title: "Thông báo",
                          message: "Xoá thất bại! Lỗi: " + error + "!",
                          type: "warning", // 'default', 'success', 'info', 'warning'
                          container: "bottom-right", // where to position the notifications
                          animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                          animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                          dismiss: {
                            pauseOnHover: true,
                            duration: 3000,
                            onScreen: true,
                          },
                        });
                      },
                    })
                  );
                } else {
                  store.addNotification({
                    title: "Thông báo",
                    message:
                      "Xoá thất bại! Người dùng không thể xoá tài khoản của mình!",
                    type: "warning", // 'default', 'success', 'info', 'warning'
                    container: "bottom-right", // where to position the notifications
                    animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                    animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                    dismiss: {
                      pauseOnHover: true,
                      onScreen: true,
                      duration: 3000,
                    },
                  });
                }
              }}
              color="success"
              simple
              className={
                classes.modalSmallFooterFirstButton +
                " " +
                classes.modalSmallFooterSecondButton
              }
            >
              Có
            </Button>
          </DialogActions>
        </Dialog>
        {/* SMALL MODAL END */}
        <Card>
          <CardHeader color="primary">
            {/* <h4 className={classes.cardTitleWhite}>Tìm kiếm sự kiện</h4> */}
            {/* <p className={classes.cardCategoryWhite}>
              Here is a subtitle for this table
            </p> */}
            <div
              className={classes.container}
              style={{
                alignItems: "center",
                marginTop: "-25px",
              }}
            >
              <UserManageSearchBox
                setisDone={setisDone}
                searchChange={(search) => {
                  if (search === "") {
                    setdetailUserParams({
                      limit: rowsPerPage,
                      page: 1,
                    });
                    // dispatch(userActions.getListUsers(""));
                  } else {
                    setdetailUserParams({
                      ...detailUserParams,
                      name: search,
                      page: 1,
                    });
                    // const detailSearch = queryString.stringify({
                    //   name: search,
                    // });
                    // dispatch(userActions.getListUsers(detailSearch));
                  }
                }}
              />
            </div>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={[
                "Tên",
                "Giới tính",
                "Ngày sinh",
                "Mã sinh viên",
                "Lớp-Khoá",
                "Khoa",
                "Quê quán",
                "Số điện thoại",
                "Email",
                "Hành động",
              ]}
              tableData={table}
            />
            <TablePagination
              component="div"
              count={totalResult}
              page={page}
              onChangePage={handleChangePage}
              rowsPerPage={rowsPerPage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </CardBody>
        </Card>
      </GridItem>
      <Popup openPopup={openPopup} title="Cập nhật tài khoản" maxWidth="lg">
        <UserManageAdd
          setopenPopup={setopenPopup}
          detail={detailEdit}
          setisDone={setisDone}
        />
      </Popup>
    </GridContainer>
  );
}

export default UserManage;
