import { makeStyles } from "@material-ui/core";
import { container } from "assets/jss/material-dashboard-react.js";
import Button from "components/CustomButtons/Button";
import CSVUpload from "components/CustomUpload/CSVUpload";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import React from "react";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch } from "react-redux";
import uploadActions from "Redux/Actions/uploadActions";
const styles = {
  container,
};

const useStyles = makeStyles(styles);

function ImportUser(props) {
  const dispatch = useDispatch();
  const { setopenPopup, setisDone } = props;
  const classes = useStyles();
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <div className={classes.container}>
          <CSVUpload
            files={(file) => {
              const fd = new FormData();
              fd.append("file", file, file.name);
              dispatch(
                uploadActions.uploadFile(fd, {
                  success: () => {
                    setisDone(true);
                    setopenPopup(false);
                    store.addNotification({
                      title: "Thông báo!",
                      message: "Tải tệp lên thành công! Đang xử lý thông tin!",
                      type: "success", // 'default', 'success', 'info', 'warning'
                      container: "bottom-right", // where to position the notifications
                      animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                      animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                      dismiss: {
                        pauseOnHover: true,
                        onScreen: true,
                        duration: 3000,
                      },
                    });
                  },
                  failed: (error) => {
                    setopenPopup(false);
                    store.addNotification({
                      title: "Thông báo!",
                      message: "Tải tệp lên thất bại! Lỗi: " + error + "!",
                      type: "warning", // 'default', 'success', 'info', 'warning'
                      container: "bottom-right", // where to position the notifications
                      animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                      animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                      dismiss: {
                        onScreen: true,
                        pauseOnHover: true,
                        duration: 5000,
                      },
                    });
                  },
                })
              );
            }}
          />
        </div>
        <div
          className={classes.container}
          style={{
            display: "flex",
            justifyContent: "center",
            marginTop: "15px",
          }}
        >
          <Button round onClick={() => setopenPopup(false)}>
            Đóng
          </Button>
        </div>
      </GridItem>
    </GridContainer>
  );
}

export default ImportUser;
