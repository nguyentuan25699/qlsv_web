import { InputAdornment, makeStyles } from "@material-ui/core";
//Icon
import SearchIcon from "@material-ui/icons/Search";
import CloseIcon from "@material-ui/icons/Close";
import AddIcon from "@material-ui/icons/Add";

import { container } from "assets/jss/material-dashboard-react.js";
import CustomInput from "components/CustomInput/CustomInput";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import React, { useState, useRef } from "react";
import Button from "components/CustomButtons/Button";
import { Link } from "react-router-dom";
import Popup from "components/Popup/Popup";
import UserManageAdd from "./UserManageAdd";
import ImportUser from "./ImportUser";

const style = {
  container,
  left: {
    float: "left!important",
    display: "block",
    padding: "15px 0 0 0",
    width: "100%",
  },
  right: {
    padding: "15px 0",
    margin: "0",
    fontSize: "14px",
    float: "right!important",
    // marginBottom: "0px",
    marginTop: "20px",
    marginBottom: "0px",
  },
};
const useStyle = makeStyles(style);

const UserManageSearchBox = (props) => {
  //!Const
  const { searchChange, setisDone } = props;
  const typingTimeOutRef = useRef(null);
  const classes = useStyle();
  //!UseState
  const [search, setsearch] = useState("");
  const [openPopup, setopenPopup] = useState(false);
  const [openPopup1, setopenPopup1] = useState(false);

  //!function
  const handleSearchChange = (params) => {
    setsearch(params.target.value);
    const value = params.target.value;
    if (!searchChange) return;
    if (typingTimeOutRef.current) {
      clearTimeout(typingTimeOutRef.current);
    }
    typingTimeOutRef.current = setTimeout(() => {
      const searchValue = value;
      searchChange(searchValue);
    }, 300);
  };
  return (
    <div className={classes.container}>
      <GridContainer>
        <GridItem xs={12} sm={12} md={3}>
          <div className={classes.container} style={{ width: "100%" }}>
            <div className={classes.left}>
              <CustomInput
                id="search"
                labelText="Tìm kiếm"
                value={search}
                style={{ color: "white" }}
                onChange={handleSearchChange}
                formControlProps={{
                  fullWidth: true,
                }}
                inputProps={{
                  endAdornment: (
                    <InputAdornment position="end">
                      {search === "" ? (
                        <SearchIcon
                          fontSize="small"
                          style={{ color: "white", padding: "3px" }}
                        />
                      ) : (
                        <Button
                          color="transparent"
                          size="lg"
                          style={{ padding: "2px" }}
                          onClick={() => {
                            setsearch("");
                            searchChange("");
                          }}
                        >
                          <CloseIcon style={{ color: "white" }} />
                        </Button>
                      )}
                    </InputAdornment>
                  ),
                  //   autoComplete: "on",
                }}
              />
            </div>
          </div>
        </GridItem>
        <GridItem xs={12} sm={12} md={9}>
          <div className={classes.right}>
            {/* <a style={{ color: "white" }} href="/main/user/add"> */}
            <Button
              round
              size="md"
              color="transparent"
              onClick={() => setopenPopup1(true)}
            >
              <AddIcon />
              Thêm nhiều tài khoản
            </Button>
            <Button
              round
              size="md"
              color="transparent"
              onClick={() => setopenPopup(true)}
            >
              <AddIcon />
              Thêm tài khoản
            </Button>
            {/* </a> */}
          </div>
        </GridItem>
      </GridContainer>
      <Popup openPopup={openPopup1} title="Thêm nhiều tài khoản">
        <ImportUser setopenPopup={setopenPopup1} setisDone={setisDone} />
      </Popup>
      <Popup openPopup={openPopup} title="Thêm tài khoản">
        <UserManageAdd setopenPopup={setopenPopup} setisDone={setisDone} />
      </Popup>
    </div>
  );
};

export default UserManageSearchBox;
