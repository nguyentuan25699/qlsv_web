import { makeStyles } from "@material-ui/core";
import { container } from "assets/jss/material-dashboard-react.js";
import customSelectStyle from "assets/jss/material-dashboard-react/customSelectStyle.js";
import Button from "components/CustomButtons/Button";
import CustomInput from "components/CustomInput/CustomInput";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import { Formik } from "formik";
import React from "react";
import { teamActions } from "Redux/Actions";
import * as yup from "yup";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch } from "react-redux";

const styles = {
  container,
  ...customSelectStyle,
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};
const useStyles = makeStyles(styles);

function TeamManageAdd(props) {
  //!Const
  const dispatch = useDispatch();
  const classes = useStyles();
  const teamSchema = yup.object({
    name: yup.string().required("Vui lòng nhập tên!"),
    code: yup.string().required("Vui lòng nhập mã!"),
    description: yup.string().required("Vui lòng nhập mô tả!"),
  });
  const { detail, setopenPopup, setisDone } = props;
  //!State
  const [detailTeam, setdetailTeam] = React.useState({
    name: "",
    description: "",
    id: "",
    code: "",
  });
  //!Useeffect
  React.useEffect(() => {
    if (!!detail && detail !== null) {
      setdetailTeam({
        ...detailTeam,
        name: detail.name,
        code: detail.code,
        description: detail.description,
      });
    }
  }, [detail]);
  //!Function
  const handleSubmit = (value) => {
    const body = {
      name: value.name,
      code: value.code,
      description: value.description,
    };
    if (!!detail && detail !== null) {
      dispatch(
        teamActions.updateTeam(body, detail.id, {
          success: () => {
            setopenPopup(false);
            setisDone(true);
            // dispatch(teamActions.getListTeam(""));
            store.addNotification({
              title: "Thông báo!",
              message: "Cập nhật khoản thành công!",
              type: "success", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                pauseOnHover: true,
                onScreen: true,
                duration: 3000,
              },
            });
            if (localStorage.getItem("id") === detail.id) {
              window.location.reload(true);
            }
          },
          failed: (error) => {
            // dispatch(teamActions.getListTeam(""));
            store.addNotification({
              title: "Thông báo!",
              message: "Cập nhật khoản thất bại! Lỗi: " + error + "!",
              type: "warning", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                onScreen: true,
                pauseOnHover: true,
                duration: 5000,
              },
            });
          },
        })
      );
    } else {
      dispatch(
        teamActions.createTeam(body, {
          success: () => {
            setopenPopup(false);
            // dispatch(teamActions.getListTeam(""));
            setisDone(true);
            store.addNotification({
              title: "Thông báo!",
              message: "Tạo khoản thành công!",
              type: "success", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                pauseOnHover: true,
                onScreen: true,
                duration: 3000,
              },
            });
          },
          failed: (error) => {
            dispatch(teamActions.getListTeam(""));
            store.addNotification({
              title: "Thông báo!",
              message: "Tạo khoản thất bại! Lỗi: " + error + "!",
              type: "warning", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                onScreen: true,
                pauseOnHover: true,
                duration: 5000,
              },
            });
          },
        })
      );
    }
  };
  //!Render
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Formik
          initialValues={detailTeam}
          enableReinitialize
          validationSchema={teamSchema}
          onSubmit={handleSubmit}
        >
          {({ values, setFieldValue, handleSubmit, errors, touched }) => {
            return (
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    id="name"
                    labelText="Tên CLB - Đội - Nhóm"
                    value={values.name}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("name", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.name && touched.name && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.name}
                    </a>
                  )}
                  <CustomInput
                    id="code"
                    labelText="Mã"
                    value={values.code}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("code", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.code && touched.code && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.code}
                    </a>
                  )}
                  <CustomInput
                    id="description"
                    labelText="Mô tả"
                    value={values.description}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("description", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.description && touched.description && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.description}
                    </a>
                  )}
                  <div
                    className={classes.container}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "10px",
                    }}
                  >
                    <Button color="success" round onClick={handleSubmit}>
                      {!!detail && detail !== null ? "Cập nhật" : "Thêm"}
                    </Button>
                    &nbsp;
                    <Button round onClick={() => setopenPopup(false)}>
                      Huỷ
                    </Button>
                  </div>
                </GridItem>
              </GridContainer>
            );
          }}
        </Formik>
      </GridItem>
    </GridContainer>
  );
}

export default TeamManageAdd;
