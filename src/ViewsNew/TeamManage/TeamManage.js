// @material-ui/core components
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  Slide,
  TablePagination,
} from "@material-ui/core";
import { Close, Edit } from "@material-ui/icons";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import Popup from "components/Popup/Popup";
import Table from "components/Table/Table.js";
import queryString from "query-string";
import React from "react";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch, useSelector } from "react-redux";
import { teamActions } from "Redux/Actions";
import TeamManageAdd from "./TeamManageAdd";
import TeamManageSearchBox from "./TeamManageSearchBox";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};
const useStyles = makeStyles(styles);
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

function TeamManage(props) {
  //!Const
  const classes = useStyles();
  const dispatch = useDispatch();
  const listTeam = useSelector((state) => state.teamReducer.listTeam);
  //!State
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [totalResult, settotalResult] = React.useState(0);
  const [isDone, setisDone] = React.useState(false);
  const [smallModal, setSmallModal] = React.useState(false);
  const [iddelete, setiddelete] = React.useState("");
  const [openPopup, setopenPopup] = React.useState(false);
  const [detailParams, setdetailParams] = React.useState({
    limit: rowsPerPage,
    page: page + 1,
  });
  const [teamEdit, setteamEdit] = React.useState({
    name: "",
    code: "",
    description: "",
  });
  const [teamTable, setteamTable] = React.useState([
    {
      name: "",
      code: "",
      description: "",
      id: "",
    },
  ]);

  //!Useeffect
  React.useEffect(() => {
    setdetailParams({
      ...detailParams,
      page: page + 1,
      limit: rowsPerPage,
    });
    // window.scrollTo(0, 0);
  }, [page, rowsPerPage]);
  // React.useEffect(() => {
  //   dispatch(
  //     teamActions.getListTeam(queryString.stringify(detailParams), {
  //       success: (data) => {
  //         console.log("1234", data);
  //         setPage(data.page - 1);
  //         settotalResult(data.totalResults);
  //       },
  //       failed: (mess) => {
  //         console.log({ mess });
  //       },
  //     })
  //   );
  // }, []);
  React.useEffect(() => {
    dispatch(
      teamActions.getListTeam(queryString.stringify(detailParams), {
        success: (data) => {
          setPage(data.page - 1);
          settotalResult(data.totalResults);
        },
        failed: (mess) => {},
      })
    );
  }, [detailParams]);
  React.useEffect(() => {
    if (isDone) {
      dispatch(
        teamActions.getListTeam(queryString.stringify(detailParams), {
          success: (data) => {
            setPage(data.page - 1);
            settotalResult(data.totalResults);
          },
          failed: (mess) => {},
        })
      );
      setisDone(false);
    }
  }, [isDone]);
  React.useEffect(() => {
    if (!!listTeam && listTeam !== null) {
      setteamTable(listTeam);
    }
  }, [listTeam]);
  //!Function
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const roundButtonsFunction = (params) => {
    const roundButtons = [
      { color: "success", icon: Edit, detail: params },
      { color: "danger", icon: Close, detail: params },
    ].map((btn, key) => {
      return btn.color === "success" ? (
        <Button
          round
          justIcon
          size="sm"
          color={btn.color}
          key={key}
          onClick={() => {
            setteamEdit({
              ...teamEdit,
              name: btn.detail.name,
              code: btn.detail.code,
              description: btn.detail.description,
              id: btn.detail.id,
            });
            setopenPopup(true);
          }}
        >
          <btn.icon />
        </Button>
      ) : (
        <Button
          round
          justIcon
          size="sm"
          color={btn.color}
          key={key}
          onClick={() => {
            setSmallModal(true);
            setiddelete(btn.detail.id);
          }}
        >
          <btn.icon />
        </Button>
      );
    });
    return roundButtons;
  };
  const table = teamTable.map((index) => {
    return [
      index.name,
      index.code,
      index.description,
      roundButtonsFunction(index),
    ];
  });
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        {/* SMALL MODAL START */}
        <Dialog
          classes={{
            root: classes.modalRoot,
            paper: classes.modal + " " + classes.modalSmall,
          }}
          open={smallModal}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => setSmallModal(false)}
          aria-labelledby="small-modal-slide-title"
          aria-describedby="small-modal-slide-description"
        >
          <DialogTitle
            id="small-modal-slide-title"
            disableTypography
            className={classes.modalHeader}
          >
            <div className={classes.container} style={{ display: "flex" }}>
              <h3 style={{ color: "red" }}>
                <b>Thông báo!</b>
              </h3>
              <Button
                simple
                className={classes.modalCloseButton}
                key="close"
                aria-label="Close"
                onClick={() => setSmallModal(false)}
              >
                <Close className={classes.modalClose} />
              </Button>
            </div>
          </DialogTitle>
          <DialogContent
            id="small-modal-slide-description"
            className={classes.modalBody + " " + classes.modalSmallBody}
          >
            <h5>Bạn có chắc chắn muốn xoá?</h5>
          </DialogContent>
          <DialogActions
            className={classes.modalFooter + " " + classes.modalFooterCenter}
          >
            <Button
              onClick={() => setSmallModal(false)}
              link
              className={classes.modalSmallFooterFirstButton}
            >
              Không
            </Button>
            <Button
              onClick={() => {
                // const id = iddelete;
                dispatch(
                  teamActions.deleteTeam(iddelete, {
                    success: () => {
                      // dispatch(teamActions.getListTeam(""));
                      setisDone(true);
                      store.addNotification({
                        title: "Thông báo",
                        message: "Xoá thành công!",
                        type: "success", // 'default', 'success', 'info', 'warning'
                        container: "bottom-right", // where to position the notifications
                        animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                        animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                        dismiss: {
                          pauseOnHover: true,
                          onScreen: true,
                          duration: 3000,
                        },
                      });
                    },
                    failed: (error) => {
                      store.addNotification({
                        title: "Thông báo",
                        message: "Xoá thất bại! Lỗi: " + error + "!",
                        type: "warning", // 'default', 'success', 'info', 'warning'
                        container: "bottom-right", // where to position the notifications
                        animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                        animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                        dismiss: {
                          pauseOnHover: true,
                          duration: 3000,
                          onScreen: true,
                        },
                      });
                    },
                  })
                );
                setSmallModal(false);
              }}
              color="success"
              simple
              className={
                classes.modalSmallFooterFirstButton +
                " " +
                classes.modalSmallFooterSecondButton
              }
            >
              Có
            </Button>
          </DialogActions>
        </Dialog>
        {/* SMALL MODAL END */}
        <Card>
          <CardHeader color="primary">
            {/* <h4 className={classes.cardTitleWhite}>Tìm kiếm sự kiện</h4> */}
            {/* <p className={classes.cardCategoryWhite}>
             Here is a subtitle for this table
           </p> */}
            <div
              className={classes.container}
              style={{
                alignItems: "center",
                marginTop: "-25px",
              }}
            >
              <TeamManageSearchBox
                setisDone={setisDone}
                searchChange={(search) => {
                  if (search === "") {
                    setdetailParams({
                      limit: rowsPerPage,
                      page: 1,
                    });
                  } else {
                    setdetailParams({
                      ...detailParams,
                      name: search,
                      page: 1,
                    });
                  }
                }}
              />
            </div>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={["Tên", "Mã", "Mô tả", "Hành động"]}
              tableData={table}
            />
            <TablePagination
              component="div"
              count={totalResult}
              page={page}
              onChangePage={handleChangePage}
              rowsPerPage={rowsPerPage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </CardBody>
        </Card>
      </GridItem>
      <Popup openPopup={openPopup} title="Cập nhật Đội - Nhóm">
        <TeamManageAdd
          setopenPopup={setopenPopup}
          detail={teamEdit}
          setisDone={setisDone}
        />
      </Popup>
    </GridContainer>
  );
}

export default TeamManage;
