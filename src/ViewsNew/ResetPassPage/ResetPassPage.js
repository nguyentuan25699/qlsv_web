import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormHelperText,
  InputAdornment,
  makeStyles,
  Slide,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";
import Icon from "@material-ui/core/Icon";
import Face from "@material-ui/icons/Face";
import loginpageStyle from "assets/jss/material-dashboard-react/views/loginpageStyle";
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";
import Button from "components/CustomButtons/Button";
import CardHeader from "components/Card/CardHeader";
import CustomInput from "components/CustomInput/CustomInput";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import React, { useState } from "react";
import Footer from "components/Footer/Footer";
import { primaryColor } from "assets/jss/material-dashboard-react";
import { Redirect } from "react-router";
import { useDispatch } from "react-redux";
import { userActions } from "Redux/Actions";
import ReactLoading from "react-loading";
import * as yup from "yup";
import { Formik } from "formik";
import queryString from "query-string";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";
const useStyle = makeStyles(loginpageStyle);
function ResetPassPage(props) {
  //!Const
  const dispatch = useDispatch();
  const resetSchema = yup.object({
    newPassword: yup
      .string()
      .required("Vui lòng nhập mật khẩu mới!")
      .matches(
        "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
        "Mật khẩu phải có ít nhất 8 kí tự, trong đó chứa ít nhất 1 kí tự và một số!"
      ),
    newPasswordRetype: yup
      .string()
      .required("Vui lòng nhập lại mật khẩu")
      .oneOf([yup.ref("newPassword"), null], "Mật khẩu không trùng khớp!"),
  });
  //!State
  const [reset, setreset] = useState({
    newPassword: "",
    newPasswordRetype: "",
  });
  const [smallModal, setSmallModal] = React.useState(false);
  const [message, setmessage] = useState("");
  const [isDone, setisDone] = useState(false);
  const [isReseting, setisReseting] = useState(false);
  const classes = useStyle();
  //!function
  const handleReset = (value) => {
    // setisReseting(true);
    // dispatch(
    const body = { password: value.newPassword };
    const param = queryString.stringify({
      token: window.location.href.split("?token=")[1],
    });
    setisReseting(true);
    dispatch(
      userActions.resetPassword(body, param, {
        success: () => {
          setmessage("");
          setisReseting(false);
          // setisDone(true);
          setSmallModal(true);
        },
        failed: (mess) => {
          setmessage(mess);
          setisReseting(false);
        },
      })
    );
    //   userActions.login(login, {
    //     success: () => {
    //       setisloged(true);
    //     },
    //     failed: (error) => {
    //       if (error.toString().includes("Incorrect studentCode or password")) {
    //         setmessage("Mã sinh viên hoặc mật khẩu không chính xác!");
    //       } else if (
    //         error
    //           .toString()
    //           .includes('"studentCode" is not allowed to be empty')
    //       ) {
    //         setmessage("Không được để trống mã sinh viên!");
    //       } else if (
    //         error.toString().includes('"password" is not allowed to be empty')
    //       ) {
    //         setmessage("Không được để trống mật khẩu!");
    //       }
    //       setisReseting(false);
    //     },
    //   })
    // );
  };
  // if (isDone) {
  //   return <Redirect to="/login" />;
  // }

  //!Render
  return (
    <div
      className={classes.pageHeaderWhite}
      style={{
        backgroundColor: "white",
        backgroundSize: "cover",
        backgroundPosition: "top center",
      }}
    >
      <div
        className={classes.container}
        style={{ marginRight: "auto", marginLeft: "auto" }}
      >
        <GridContainer justify="center">
          {/* SMALL MODAL START */}
          <Dialog
            classes={{
              root: classes.modalRoot,
              paper: classes.modal + " " + classes.modalSmall,
            }}
            open={smallModal}
            TransitionComponent={Transition}
            keepMounted
            onClose={() => setSmallModal(false)}
            aria-labelledby="small-modal-slide-title"
            aria-describedby="small-modal-slide-description"
          >
            <DialogTitle
              id="small-modal-slide-title"
              disableTypography
              className={classes.modalHeader}
            >
              <div className={classes.container} style={{ display: "flex" }}>
                <h3 style={{ color: "red" }}>
                  <b>Thông báo!</b>
                </h3>
                <a href="/login">
                  <Button
                    simple
                    className={classes.modalCloseButton}
                    key="close"
                    aria-label="Close"
                    onClick={() => {
                      setSmallModal(false);
                    }}
                  >
                    <Close className={classes.modalClose} />
                  </Button>
                </a>
              </div>
            </DialogTitle>
            <DialogContent
              id="small-modal-slide-description"
              className={classes.modalBody + " " + classes.modalSmallBody}
            >
              <h5>Cấp lại mật khẩu thành công!</h5>
            </DialogContent>
            <DialogActions
              className={classes.modalFooter + " " + classes.modalFooterCenter}
            >
              <a href="/login">
                <Button
                  onClick={() => {
                    setSmallModal(false);
                    // setisDone(true);
                  }}
                  link
                  className={classes.modalSmallFooterFirstButton}
                >
                  Ok
                </Button>
              </a>
            </DialogActions>
          </Dialog>
          {/* SMALL MODAL END */}
          <GridItem xs={12} sm={12} md={10}>
            <Formik
              initialValues={reset}
              onSubmit={handleReset}
              enableReinitialize
              validationSchema={resetSchema}
            >
              {({ values, setFieldValue, handleSubmit, errors, touched }) => {
                return (
                  <Card style={{ maxWidth: "500px" }}>
                    <CardHeader className={classes.cardHeader} color="primary">
                      <h4>CẤP LẠI MẬT KHẨU</h4>
                    </CardHeader>
                    <p
                      className={classes.description + " " + classes.textCenter}
                    >
                      Vui lòng điền đầy đủ thông tin
                    </p>
                    <CardBody>
                      <CustomInput
                        required
                        id="newPass"
                        value={values.newPassword}
                        onChange={(text) => {
                          setFieldValue("newPassword", text.target.value);
                        }}
                        formControlProps={{
                          fullWidth: true,
                        }}
                        inputProps={{
                          placeholder: "Mật khẩu mới",
                          type: "password",
                          startAdornment: (
                            <InputAdornment position="start">
                              <Icon className={classes.inputIconsColor}>
                                lock_utline
                              </Icon>
                            </InputAdornment>
                          ),
                          autoComplete: "on",
                        }}
                      />
                      {errors.newPassword && touched.newPassword && (
                        <a
                          style={{
                            fontSize: 12,
                            color: "red",
                          }}
                        >
                          {errors.newPassword}
                        </a>
                      )}
                      <CustomInput
                        required
                        id="newPassRetype"
                        value={values.newPasswordRetype}
                        onChange={(text) => {
                          setFieldValue("newPasswordRetype", text.target.value);
                        }}
                        formControlProps={{
                          fullWidth: true,
                        }}
                        inputProps={{
                          placeholder: "Nhập lại mật khẩu",
                          type: "password",
                          startAdornment: (
                            <InputAdornment position="start">
                              <Icon className={classes.inputIconsColor}>
                                lock_utline
                              </Icon>
                            </InputAdornment>
                          ),
                          autoComplete: "on",
                        }}
                      />
                      {errors.newPasswordRetype && touched.newPasswordRetype && (
                        <a
                          style={{
                            fontSize: 12,
                            color: "red",
                          }}
                        >
                          {errors.newPasswordRetype}
                        </a>
                      )}
                    </CardBody>
                    <div className={classes.textCenter}>
                      <div
                        className={classes.container}
                        style={{ justifyContent: "center" }}
                      >
                        {message !== "" ? (
                          <FormHelperText
                            style={{
                              marginTop: -5,
                              color: "red",
                              textAlign: "center",
                            }}
                          >
                            {message}
                          </FormHelperText>
                        ) : null}
                      </div>
                      <div
                        className={classes.container}
                        style={{
                          justifyContent: "center",
                          paddingBottom: "10px",
                          display: "flex",
                        }}
                      >
                        {isReseting ? (
                          <ReactLoading
                            type="spin"
                            color="black"
                            height="30px"
                            width="30px"
                          />
                        ) : (
                          <Button
                            color="primary"
                            size="lg"
                            onClick={handleSubmit}
                            // href={isloged ? "/" : null}
                            className={classes.navLink}
                          >
                            Xác nhận
                          </Button>
                        )}
                        <br />
                      </div>

                      <div
                        className={classes.container}
                        style={{
                          justifyContent: "center",
                          paddingBottom: "10px",
                          display: "flex",
                        }}
                      >
                        <span>
                          Hoặc{" "}
                          <a
                            href="login"
                            style={{
                              fontStyle: "italic",
                              color: primaryColor[0],
                            }}
                          >
                            Đăng nhập?
                          </a>
                        </span>
                        <br />
                      </div>
                    </div>
                  </Card>
                );
              }}
            </Formik>
          </GridItem>
        </GridContainer>
      </div>
      <Footer
        className={classes.footer}
        style={{ justifyContent: "flex-end" }}
      />
    </div>
  );
}

export default ResetPassPage;
