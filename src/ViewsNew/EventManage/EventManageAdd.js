import { FormControl, makeStyles, TextField } from "@material-ui/core";
import { container } from "assets/jss/material-dashboard-react.js";
import customSelectStyle from "assets/jss/material-dashboard-react/customSelectStyle.js";
import Button from "components/CustomButtons/Button";
import CustomInput from "components/CustomInput/CustomInput";
import ImageUpload from "components/CustomUpload/ImageUpload";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import { Formik } from "formik";
import moment from "moment";
import React from "react";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch, useSelector } from "react-redux";
import { uploadActions } from "Redux/Actions";
import eventActions from "Redux/Actions/eventActions";
import { BASE_URL_IMAGE } from "Service/ServiceURL";
import * as yup from "yup";
import queryString from "query-string";

const styles = {
  container,
  ...customSelectStyle,
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};
const useStyles = makeStyles(styles);

const EventManageAdd = (props) => {
  //!Const
  const dispatch = useDispatch();
  const classes = useStyles();
  const imageLink = useSelector((state) => state.uploadReducer.imageLink);
  const eventSchema = yup.object({
    name: yup.string().required("Vui lòng nhập tên sự kiện!"),
    score: yup
      .string()
      .matches("^\\d+$", "Điểm sự kiện phải là số nguyên dương!")
      .required("Vui lòng nhập điểm sự kiện!"),
    endTime: yup.string().required("Vui lòng chọn thời gian kết thúc sự kiện!"),
    startTime: yup
      .string()
      .required("Vui lòng chọn thời gian bắt đầu sự kiện!"),
    endTimeRegister: yup
      .string()
      .required("Vui lòng chọn thời gian kết thúc đăng ký sự kiện!"),
    startTimeRegister: yup
      .string()
      .required("Vui lòng chọn thời gian bắt đầu đăng ký sự kiện!"),
    description: yup.string().required("Vui lòng nhập mô tả sự kiện!"),
    address: yup.string().required("Vui lòng nhập địa chỉ sự kiện!"),
  });
  const { detail, setopenPopup, setisDone } = props;
  //!State
  const [detailEvent, setdetailEvent] = React.useState({
    name: "",
    score: "",
    endTime: "",
    startTime: "",
    endTimeRegister: "",
    startTimeRegister: "",
    description: "",
    address: "",
    id: "",
    image: "",
  });

  //!Useeffect
  React.useEffect(() => {
    if (!!detail && detail !== null) {
      setdetailEvent({
        ...detailEvent,
        name: detail.name,
        description: detail.description,
        score: detail.score,
        endTime: detail.endTime,
        startTime: detail.startTime,
        endTimeRegister: detail.endTimeRegister,
        startTimeRegister: detail.startTimeRegister,
        id: detail.id,
        address: detail.address,
        image: detail.image,
      });
    }
  }, [detail]);

  //!Function
  const handleDetailAdd = (value) => {
    const body = {
      name: value.name,
      description: value.description,
      score: value.score,
      endTime: value.endTime,
      startTime: value.startTime,
      endTimeRegister: value.endTimeRegister,
      startTimeRegister: value.startTimeRegister,
      address: value.address,
      image: value.image,
    };
    if (detail === undefined) {
      dispatch(
        eventActions.createEvent(body, {
          success: () => {
            setopenPopup(false);
            store.addNotification({
              title: "Thông báo!",
              message: "Thêm kiện thành công!",
              type: "success", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                pauseOnHover: true,
                onScreen: true,
                duration: 3000,
              },
            });
            setisDone(true);
          },
          failed: (error) => {
            store.addNotification({
              title: "Thông báo!",
              message: "Thêm kiện thất bại! Lỗi: " + error + "!",
              type: "warning", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                onScreen: true,
                pauseOnHover: true,
                duration: 5000,
              },
            });
          },
        })
      );
    } else {
      dispatch(
        eventActions.updateEvent(body, detail.id, {
          success: () => {
            setopenPopup(false);
            setisDone(true);
            store.addNotification({
              title: "Thông báo!",
              message: "Cập nhật kiện thành công!",
              type: "success", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                pauseOnHover: true,
                onScreen: true,
                duration: 3000,
              },
            });
          },
          failed: (error) => {
            store.addNotification({
              title: "Thông báo!",
              message: "Cập nhật kiện thất bại! Lỗi: " + error + "!",
              type: "warning", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                onScreen: true,
                pauseOnHover: true,
                duration: 5000,
              },
            });
          },
        })
      );
    }
  };
  //!Render
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Formik
          initialValues={detailEvent}
          enableReinitialize
          validationSchema={eventSchema}
          onSubmit={handleDetailAdd}
        >
          {({ values, setFieldValue, handleSubmit, errors, touched }) => {
            return (
              <GridContainer>
                <GridItem xs={12} sm={12} md={4}>
                  <div
                    className={classes.container}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      width: "50%",
                      height: "auto",
                    }}
                  >
                    <ImageUpload
                      avatar={
                        values.image !== "" ? BASE_URL_IMAGE + values.image : ""
                      }
                      files={(file) => {
                        const fd = new FormData();
                        fd.append("image", file, file.name);
                        dispatch(
                          uploadActions.uploadImage(fd, {
                            success: (img) => {
                              setFieldValue("image", img);
                              store.addNotification({
                                title: "Thông báo!",
                                message: "Chọn và tải ảnh lên thành công!",
                                type: "success", // 'default', 'success', 'info', 'warning'
                                container: "bottom-right", // where to position the notifications
                                animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                                animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                                dismiss: {
                                  pauseOnHover: true,
                                  onScreen: true,
                                  duration: 3000,
                                },
                              });
                            },
                            failed: (mess) => {
                              store.addNotification({
                                title: "Thông báo!",
                                message:
                                  "Chọn và tải ảnh lên thất bại! Lỗi: " +
                                  mess +
                                  "!",
                                type: "warning", // 'default', 'success', 'info', 'warning'
                                container: "bottom-right", // where to position the notifications
                                animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                                animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                                dismiss: {
                                  onScreen: true,
                                  pauseOnHover: true,
                                  duration: 5000,
                                },
                              });
                            },
                          })
                        );
                      }}
                    />
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={8}>
                  <CustomInput
                    id="name"
                    labelText="Tên sự kiện"
                    value={values.name}
                    onChange={(text) =>
                      setFieldValue("name", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.name && touched.name && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.name}
                    </a>
                  )}
                  <CustomInput
                    id="description"
                    labelText="Mô tả sự kiện"
                    value={values.description}
                    onChange={(text) =>
                      setFieldValue("description", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                      multiline: true,
                      rows: 4,
                    }}
                  />
                  {errors.description && touched.description && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.description}
                    </a>
                  )}
                  <CustomInput
                    id="address"
                    labelText="Địa điểm tổ chức"
                    value={values.address}
                    onChange={(text) =>
                      setFieldValue("address", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.address && touched.address && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.address}
                    </a>
                  )}
                  <CustomInput
                    id="score"
                    labelText="Điểm sự kiện"
                    value={values.score}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("score", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      autoComplete: "off",
                    }}
                  />
                  {errors.score && touched.score && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.score}
                    </a>
                  )}
                  <FormControl fullWidth className={classes.selectFormControl}>
                    <TextField
                      id="datetime-local"
                      label="Thời gian bắt đầu sự kiện"
                      type="datetime-local"
                      defaultValue={moment(new Date()).format(
                        "YYYY-MM-DDTHH:mm"
                      )}
                      value={
                        !!values.startTime && values.startTime !== ""
                          ? moment(values.startTime).format("YYYY-MM-DDTHH:mm")
                          : null
                      }
                      className={classes.textField}
                      onChange={(time) => {
                        setFieldValue(
                          "startTime",
                          new Date(time.target.value).toISOString()
                        );
                      }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.startTime && touched.startTime && (
                      <a
                        style={{
                          fontSize: 12,
                          color: "red",
                        }}
                      >
                        {errors.startTime}
                      </a>
                    )}
                  </FormControl>
                  <FormControl fullWidth className={classes.selectFormControl}>
                    <TextField
                      id="datetime-local"
                      label="Thời gian kết thúc sự kiện"
                      type="datetime-local"
                      defaultValue={moment(new Date()).format(
                        "YYYY-MM-DDTHH:mm"
                      )}
                      value={
                        !!values.endTime && values.endTime !== ""
                          ? moment(values.endTime).format("YYYY-MM-DDTHH:mm")
                          : null
                      }
                      className={classes.textField}
                      onChange={(time) => {
                        setFieldValue(
                          "endTime",
                          new Date(time.target.value).toISOString()
                        );
                      }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.endTime && touched.endTime && (
                      <a
                        style={{
                          fontSize: 12,
                          color: "red",
                        }}
                      >
                        {errors.endTime}
                      </a>
                    )}
                  </FormControl>
                  <FormControl fullWidth className={classes.selectFormControl}>
                    <TextField
                      id="datetime-local"
                      label="Thời gian bắt đầu đăng ký sự kiện"
                      type="datetime-local"
                      defaultValue={moment(new Date()).format(
                        "YYYY-MM-DDTHH:mm"
                      )}
                      value={
                        !!values.startTimeRegister &&
                        values.startTimeRegister !== ""
                          ? moment(values.startTimeRegister).format(
                              "YYYY-MM-DDTHH:mm"
                            )
                          : null
                      }
                      className={classes.textField}
                      onChange={(time) => {
                        setFieldValue(
                          "startTimeRegister",
                          new Date(time.target.value).toISOString()
                        );
                      }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.startTimeRegister && touched.startTimeRegister && (
                      <a
                        style={{
                          fontSize: 12,
                          color: "red",
                        }}
                      >
                        {errors.startTimeRegister}
                      </a>
                    )}
                  </FormControl>
                  <FormControl fullWidth className={classes.selectFormControl}>
                    <TextField
                      id="datetime-local"
                      label="Thời gian kết thúc đăng ký sự kiện"
                      type="datetime-local"
                      defaultValue={moment(new Date()).format(
                        "YYYY-MM-DDTHH:mm"
                      )}
                      value={
                        !!values.endTimeRegister &&
                        values.endTimeRegister !== ""
                          ? moment(values.endTimeRegister).format(
                              "YYYY-MM-DDTHH:mm"
                            )
                          : null
                      }
                      className={classes.textField}
                      onChange={(time) => {
                        setFieldValue(
                          "endTimeRegister",
                          new Date(time.target.value).toISOString()
                        );
                      }}
                      InputLabelProps={{
                        shrink: true,
                      }}
                    />
                    {errors.endTimeRegister && touched.endTimeRegister && (
                      <a
                        style={{
                          fontSize: 12,
                          color: "red",
                        }}
                      >
                        {errors.endTimeRegister}
                      </a>
                    )}
                  </FormControl>
                  <div
                    className={classes.container}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "10px",
                    }}
                  >
                    <Button color="success" round onClick={handleSubmit}>
                      {!!detail && detail !== null ? "Cập nhật" : "Thêm"}
                    </Button>
                    &nbsp;
                    <Button round onClick={() => setopenPopup(false)}>
                      Huỷ
                    </Button>
                  </div>
                </GridItem>
              </GridContainer>
            );
          }}
        </Formik>
      </GridItem>
    </GridContainer>
  );
};

export default EventManageAdd;
