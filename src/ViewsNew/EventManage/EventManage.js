// @material-ui/core components
import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  makeStyles,
  Slide,
  TablePagination,
} from "@material-ui/core";
import { Close, Edit } from "@material-ui/icons";
import Card from "components/Card/Card.js";
import CardBody from "components/Card/CardBody.js";
import CardHeader from "components/Card/CardHeader.js";
import Button from "components/CustomButtons/Button";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import Popup from "components/Popup/Popup";
import Table from "components/Table/Table.js";
import moment from "moment";
import React from "react";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch, useSelector } from "react-redux";
import AssignmentIcon from "@material-ui/icons/Assignment";
import eventActions from "Redux/Actions/eventActions";
import EventManageAdd from "./EventManageAdd";
import EventManageSearchBox from "./EventManageSearchBox";
import queryString from "query-string";
import EventDetail from "./EventDetail";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { parseInt } from "lodash-es";

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0",
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF",
    },
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1",
    },
  },
};
const useStyles = makeStyles(styles);
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

function EventManage(props) {
  //!Const
  const classes = useStyles();
  const listEvent = useSelector((state) => state.eventReducer.listEvent);
  const dispatch = useDispatch();
  //!State
  const [isDone, setisDone] = React.useState(false);
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [totalResult, settotalResult] = React.useState(0);
  const [smallModal, setSmallModal] = React.useState(false);
  const [iddelete, setiddelete] = React.useState("");
  const [Detail, setDetail] = React.useState({});
  const [DetailTable, setDetailTable] = React.useState({
    limit: rowsPerPage,
    page: page + 1,
    sortBy: "startTime:desc",
  });
  const [openPopup, setopenPopup] = React.useState(false);
  const [openPopup1, setopenPopup1] = React.useState(false);
  const [status, setstatus] = React.useState({
    success: false,
    failed: false,
  });
  const [eventEdit, seteventEdit] = React.useState({});
  const [eventTable, seteventTable] = React.useState([{}]);

  //!Function
  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const roundButtonsFunction = (params) => {
    const roundButtons = [
      { color: "warning", icon: VisibilityIcon, detail: params },
      { color: "success", icon: Edit, detail: params },
      { color: "danger", icon: Close, detail: params },
    ].map((btn, key) => {
      if (btn.color === "success") {
        return (
          <Button
            round
            justIcon
            size="sm"
            color={btn.color}
            key={key}
            onClick={() => {
              seteventEdit({
                id: btn.detail.id,
                name: btn.detail.name,
                score: btn.detail.score,
                startTime: btn.detail.startTime,
                endTime: btn.detail.endTime,
                startTimeRegister: btn.detail.startTimeRegister,
                endTimeRegister: btn.detail.endTimeRegister,
                description: btn.detail.description,
                image: btn.detail.image,
                address: btn.detail.address,
              });
              setopenPopup(true);
            }}
          >
            <btn.icon />
          </Button>
        );
      } else if (btn.color === "danger") {
        return (
          <Button
            round
            justIcon
            size="sm"
            color={btn.color}
            key={key}
            onClick={() => {
              setiddelete(btn.detail.id);
              // setopenPopup(true);
              setSmallModal(true);
            }}
          >
            <btn.icon />
          </Button>
        );
      } else {
        return (
          <Button
            round
            justIcon
            size="sm"
            color={btn.color}
            key={key}
            onClick={() => {
              // setidDetail(btn.detail.id);
              setDetail(btn.detail);
              setopenPopup1(true);
            }}
          >
            <btn.icon />
          </Button>
        );
      }
    });
    return roundButtons;
  };
  //!Use effect
  React.useEffect(() => {
    setDetailTable({
      ...DetailTable,
      page: page + 1,
      limit: rowsPerPage,
    });
    // window.scrollTo(0, 0);
  }, [page, rowsPerPage]);
  React.useEffect(() => {
    dispatch(
      eventActions.getListEvent(queryString.stringify(DetailTable), {
        success: (data) => {
          setPage(data.page - 1);
          settotalResult(data.totalResults);
        },
        failed: (data) => {},
      })
    );
  }, []);
  React.useEffect(() => {
    dispatch(
      eventActions.getListEvent(queryString.stringify(DetailTable), {
        success: (data) => {
          setPage(data.page - 1);
          settotalResult(data.totalResults);
        },
        failed: (data) => {},
      })
    );
  }, [status, DetailTable]);
  React.useEffect(() => {
    if (isDone) {
      dispatch(
        eventActions.getListEvent(queryString.stringify(DetailTable), {
          success: (data) => {
            setPage(data.page - 1);
            settotalResult(data.totalResults);
          },
          failed: (data) => {},
        })
      );
      setisDone(false);
    }
  }, [isDone]);
  React.useEffect(() => {
    if (!!listEvent) {
      seteventTable(listEvent);
    }
  }, [listEvent]);
  const table = eventTable.map((index) => {
    return [
      index.name,
      index.status,
      moment(index.startTimeRegister).format("HH:mm DD/MM/YYYY") +
        " - " +
        moment(index.endTimeRegister).format("HH:mm DD/MM/YYYY"),
      moment(index.startTime).format("HH:mm DD/MM/YYYY") +
        " - " +
        moment(index.endTime).format("HH:mm DD/MM/YYYY"),
      index.score,
      index.address,
      roundButtonsFunction(index),
    ];
  });
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        {/* SMALL MODAL START */}
        <Dialog
          classes={{
            root: classes.modalRoot,
            paper: classes.modal + " " + classes.modalSmall,
          }}
          open={smallModal}
          TransitionComponent={Transition}
          keepMounted
          onClose={() => setSmallModal(false)}
          aria-labelledby="small-modal-slide-title"
          aria-describedby="small-modal-slide-description"
        >
          <DialogTitle
            id="small-modal-slide-title"
            disableTypography
            className={classes.modalHeader}
          >
            <div className={classes.container} style={{ display: "flex" }}>
              <h3 style={{ color: "red" }}>
                <b>Thông báo!</b>
              </h3>
              <Button
                simple
                className={classes.modalCloseButton}
                key="close"
                aria-label="Close"
                onClick={() => setSmallModal(false)}
              >
                <Close className={classes.modalClose} />
              </Button>
            </div>
          </DialogTitle>
          <DialogContent
            id="small-modal-slide-description"
            className={classes.modalBody + " " + classes.modalSmallBody}
          >
            <h5>Bạn có chắc chắn muốn xoá sự kiện?</h5>
          </DialogContent>
          <DialogActions
            className={classes.modalFooter + " " + classes.modalFooterCenter}
          >
            <Button
              onClick={() => setSmallModal(false)}
              link
              className={classes.modalSmallFooterFirstButton}
            >
              Không
            </Button>
            <Button
              onClick={() => {
                const id = iddelete;
                dispatch(
                  eventActions.deleteEvent(id, {
                    success: () => {
                      setstatus({
                        success: true,
                        failed: false,
                      });
                      setisDone(true);
                      store.addNotification({
                        title: "Thông báo",
                        message: "Xoá thành công!",
                        type: "success", // 'default', 'success', 'info', 'warning'
                        container: "bottom-right", // where to position the notifications
                        animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                        animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                        dismiss: {
                          pauseOnHover: true,
                          onScreen: true,
                          duration: 3000,
                        },
                      });
                    },
                    failed: (error) => {
                      setstatus({
                        success: false,
                        failed: true,
                      });
                      store.addNotification({
                        title: "Thông báo",
                        message: "Xoá thất bại! Lỗi: " + error + "!",
                        type: "warning", // 'default', 'success', 'info', 'warning'
                        container: "bottom-right", // where to position the notifications
                        animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                        animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                        dismiss: {
                          pauseOnHover: true,
                          duration: 3000,
                        },
                      });
                    },
                  })
                );
                setSmallModal(false);
              }}
              color="success"
              simple
              className={
                classes.modalSmallFooterFirstButton +
                " " +
                classes.modalSmallFooterSecondButton
              }
            >
              Có
            </Button>
          </DialogActions>
        </Dialog>
        {/* SMALL MODAL END */}
        <Card>
          <CardHeader color="primary">
            {/* <h4 className={classes.cardTitleWhite}>Tìm kiếm sự kiện</h4> */}
            {/* <p className={classes.cardCategoryWhite}>
              Here is a subtitle for this table
            </p> */}
            <div
              className={classes.container}
              style={{
                alignItems: "center",
                marginTop: "-25px",
              }}
            >
              <EventManageSearchBox
                searchChange={(search) => {
                  if (search === "") {
                    setDetailTable({
                      limit: rowsPerPage,
                      page: page + 1,
                      sortBy: "startTime:desc",
                    });
                    // dispatch(
                    //   eventActions.getListEvent(
                    //     queryString.stringify(DetailTable)
                    //   )
                    // );
                  } else {
                    const detailSearch = queryString.stringify({
                      name: search,
                    });
                    setDetailTable({ ...DetailTable, name: search });
                    // dispatch(eventActions.getListEvent(DetailTable));
                  }
                }}
                setisDone={setisDone}
              />
            </div>
          </CardHeader>
          <CardBody>
            <Table
              tableHeaderColor="primary"
              tableHead={[
                "Tên sự kiện",
                "Trạng thái",
                "Thời gian đăng ký",
                "Thời gian sự kiện",
                "Điểm sự kiện",
                "Địa điểm sự kiện",
                "Hành động",
              ]}
              tableData={table}
            />
            <TablePagination
              component="div"
              count={totalResult}
              page={page}
              onChangePage={handleChangePage}
              rowsPerPage={rowsPerPage}
              onChangeRowsPerPage={handleChangeRowsPerPage}
            />
          </CardBody>
        </Card>
      </GridItem>
      <Popup openPopup={openPopup} title="Cập nhật sự kiện">
        <EventManageAdd
          setopenPopup={setopenPopup}
          setisDone={setisDone}
          detail={eventEdit}
        />
      </Popup>
      <Popup openPopup={openPopup1} title="Danh sách sinh viên với sự kiện">
        <EventDetail setopenPopup={setopenPopup1} detail={Detail} />
      </Popup>
    </GridContainer>
  );
}

export default EventManage;
