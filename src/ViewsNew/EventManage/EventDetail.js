import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  InputAdornment,
  InputLabel,
  makeStyles,
  MenuItem,
  Select,
  Slide,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";
import AddIcon from "@material-ui/icons/Add";
import CloseIcon from "@material-ui/icons/Close";
import SearchIcon from "@material-ui/icons/Search";
import { container } from "assets/jss/material-dashboard-react";
import customSelectStyle from "assets/jss/material-dashboard-react/customSelectStyle.js";
import Button from "components/CustomButtons/Button";
import CustomInput from "components/CustomInput/CustomInput";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import Popup from "components/Popup/Popup";
import Table from "components/Table/Table.js";
import moment from "moment";
import queryString from "query-string";
import React from "react";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch, useSelector } from "react-redux";
import { eventJoinActions } from "Redux/Actions";
import EventDetailAdd from "./EventDetailAdd";

const useStyle = makeStyles({
  ...customSelectStyle,
  container,
  left: {
    padding: "15px 0 0 0",
    float: "left!important",
    display: "block",
    width: "100%",
  },
  right: {
    padding: "15px 0",
    margin: "0",
    fontSize: "14px",
    float: "right!important",
    // marginBottom: "0px",
    marginTop: "20px",
    marginBottom: "0px",
  },
});
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";

function EventDetail(props) {
  //!Const
  const classes = useStyle();
  const dispatch = useDispatch();
  const { setopenPopup, detail } = props;
  const listEventJoin = useSelector(
    (state) => state.eventJoinReducer.listEventJoin
  );
  //!State
  const [smallModal, setSmallModal] = React.useState(false);
  const [openPopup1, setopenPopup1] = React.useState(false);
  const [iddelete, setiddelete] = React.useState("");
  const [filter, setfilter] = React.useState([
    {
      text: "Sinh viên đăng ký",
      value: "Đã Đăng ký",
    },
    {
      text: "Sinh viên đã tham gia",
      value: "Đã Tham gia",
    },
  ]);
  const [detailEvent, setdetailEvent] = React.useState({
    eventId: detail.id,
    status: "Đã Đăng ký",
    populate: "eventId,userId",
  });
  const [joinSearch, setjoinSearch] = React.useState("");
  const [tableDetail, settableDetail] = React.useState([]);
  const [registerSearch, setregisterSearch] = React.useState("");
  //!Function
  const buttonFunction = (params) => {
    const roundButtons = [{ color: "danger", icon: Close, detail: params }].map(
      (btn, key) => {
        return (
          <Button
            round
            justIcon
            size="sm"
            color={btn.color}
            key={key}
            onClick={() => {
              setSmallModal(true);
              setiddelete(btn.detail.id);
            }}
          >
            <btn.icon />
          </Button>
        );
      }
    );
    return roundButtons;
  };
  //!Use effect
  React.useEffect(() => {
    dispatch(
      eventJoinActions.getListEventJoin(queryString.stringify(detailEvent))
    );
  }, []);
  React.useEffect(() => {
    dispatch(
      eventJoinActions.getListEventJoin(queryString.stringify(detailEvent))
    );
  }, [detailEvent]);
  React.useEffect(() => {
    settableDetail(listEventJoin);
  }, [listEventJoin]);
  React.useEffect(() => {
    dispatch(
      eventJoinActions.getListEventJoin(queryString.stringify(detailEvent))
    );
  }, [detailEvent]);
  const table =
    detailEvent.status === "Đã Đăng ký"
      ? tableDetail.map((index) => {
          return [
            index.userId.name,
            index.userId.studentCode,
            index.userId.class + " - K" + index.userId.schoolYear,
            index.userId.faculty,
            moment(index.timeRegister).format("hh:mm DD/MM/YYYY"),
            buttonFunction(index),
          ];
        })
      : tableDetail.map((index) => {
          return [
            index.userId.name,
            index.userId.studentCode,
            index.userId.class + " - K" + index.userId.schoolYear,
            index.userId.faculty,
            moment(index.timeJoin).format("hh:mm DD/MM/YYYY"),
            buttonFunction(index),
          ];
        });

  return (
    <GridContainer>
      {/* SMALL MODAL START */}
      <Dialog
        classes={{
          root: classes.modalRoot,
          paper: classes.modal + " " + classes.modalSmall,
        }}
        open={smallModal}
        TransitionComponent={Transition}
        keepMounted
        onClose={() => setSmallModal(false)}
        aria-labelledby="small-modal-slide-title"
        aria-describedby="small-modal-slide-description"
      >
        <DialogTitle
          id="small-modal-slide-title"
          disableTypography
          className={classes.modalHeader}
        >
          <div className={classes.container} style={{ display: "flex" }}>
            <h3 style={{ color: "red" }}>
              <b>Thông báo!</b>
            </h3>
            <Button
              simple
              className={classes.modalCloseButton}
              key="close"
              aria-label="Close"
              onClick={() => setSmallModal(false)}
            >
              <Close className={classes.modalClose} />
            </Button>
          </div>
        </DialogTitle>
        <DialogContent
          id="small-modal-slide-description"
          className={classes.modalBody + " " + classes.modalSmallBody}
        >
          <h5>Bạn có chắc chắn muốn xoá?</h5>
        </DialogContent>
        <DialogActions
          className={classes.modalFooter + " " + classes.modalFooterCenter}
        >
          <Button
            onClick={() => setSmallModal(false)}
            link
            className={classes.modalSmallFooterFirstButton}
          >
            Không
          </Button>
          <Button
            onClick={() => {
              dispatch(
                eventJoinActions.deleteEventJoin(iddelete, {
                  success: () => {
                    dispatch(
                      eventJoinActions.getListEventJoin(
                        queryString.stringify(detailEvent)
                      )
                    );
                    // dispatch(userActions.getListUsers(""));
                    store.addNotification({
                      title: "Thông báo",
                      message: "Xoá thành công!",
                      type: "success", // 'default', 'success', 'info', 'warning'
                      container: "bottom-right", // where to position the notifications
                      animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                      animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                      dismiss: {
                        pauseOnHover: true,
                        onScreen: true,
                        duration: 3000,
                      },
                    });
                  },
                  failed: (error) => {
                    store.addNotification({
                      title: "Thông báo",
                      message: "Xoá thất bại! Lỗi: " + error + "!",
                      type: "warning", // 'default', 'success', 'info', 'warning'
                      container: "bottom-right", // where to position the notifications
                      animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
                      animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
                      dismiss: {
                        pauseOnHover: true,
                        duration: 3000,
                        onScreen: true,
                      },
                    });
                  },
                })
              );
              setSmallModal(false);
            }}
            color="success"
            simple
            className={
              classes.modalSmallFooterFirstButton +
              " " +
              classes.modalSmallFooterSecondButton
            }
          >
            Có
          </Button>
        </DialogActions>
      </Dialog>
      {/* SMALL MODAL END */}
      <GridItem xs={12} sm={12} md={4}>
        {detailEvent.status === "Đã Đăng ký" ? (
          <CustomInput
            id="search"
            labelText="Tìm kiếm sinh viên đã đăng ký"
            value={joinSearch}
            //   style={{ color: "white" }}
            onChange={(params) => {
              setjoinSearch(params.target.value);
            }}
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  {joinSearch === "" ? (
                    <SearchIcon fontSize="small" style={{ padding: "3px" }} />
                  ) : (
                    <Button
                      color="transparent"
                      size="lg"
                      style={{ padding: "2px" }}
                      onClick={() => {
                        setjoinSearch("");
                        //   searchChange("");
                      }}
                    >
                      <CloseIcon size="sm" />
                    </Button>
                  )}
                </InputAdornment>
              ),
              //   autoComplete: "on",
            }}
          />
        ) : (
          <CustomInput
            id="search"
            labelText="Tìm kiếm sinh viên đã tham gia"
            value={registerSearch}
            //   style={{ color: "white" }}
            onChange={(params) => {
              setregisterSearch(params.target.value);
            }}
            formControlProps={{
              fullWidth: true,
            }}
            inputProps={{
              endAdornment: (
                <InputAdornment position="end">
                  {joinSearch === "" ? (
                    <SearchIcon fontSize="small" style={{ padding: "3px" }} />
                  ) : (
                    <Button
                      color="transparent"
                      size="lg"
                      style={{ padding: "2px" }}
                      onClick={() => {
                        setregisterSearch("");
                        //   searchChange("");
                      }}
                    >
                      <CloseIcon size="sm" />
                    </Button>
                  )}
                </InputAdornment>
              ),
              //   autoComplete: "on",
            }}
          />
        )}
      </GridItem>
      <GridItem xs={12} sm={12} md={4}></GridItem>
      <GridItem xs={12} sm={12} md={4}>
        <FormControl
          fullWidth
          className={classes.selectFormControl}
          style={{ minWidth: "200px" }}
        >
          <InputLabel htmlFor="simple-select" className={classes.selectLabel}>
            Danh sách
          </InputLabel>
          <Select
            MenuProps={{
              className: classes.selectMenu,
            }}
            classes={{
              select: classes.select,
            }}
            value={detailEvent.status}
            onChange={(text) => {
              //   setFieldValue("gender", text.target.value);
              setdetailEvent({ ...detailEvent, status: text.target.value });
            }}
            inputProps={{
              name: "simpleSelect",
              id: "simple-select",
            }}
          >
            {filter.map((cate, key) => {
              return (
                <MenuItem
                  key={key}
                  classes={{
                    root: classes.selectMenuItem,
                    selected: classes.selectMenuItemSelected,
                  }}
                  value={cate.value}
                >
                  {cate.text}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </GridItem>
      <GridItem xs={12} sm={12} md={8}>
        <div className={classes.container} style={{ flexWrap: "wrap" }}>
          <div style={{ padding: "20px 0 0 0", flexWrap: "wrap" }}>
            <b>Tên sự kiện: </b>
            {detail.name}
          </div>
          <div style={{ padding: "5px 0 10px 0" }}>
            <b>Thời gian: </b>
            {moment(detail.startTime).format("DD/MM/YYYY hh:mm") +
              " - " +
              moment(detail.endTime).format("DD/MM/YYYY hh:mm")}
            &nbsp;
          </div>
        </div>
      </GridItem>
      <GridItem xs={12} sm={12} md={4}>
        <div className={classes.right}>
          {detailEvent.status === "Đã Đăng ký" ? null : (
            <Button
              round
              size="md"
              color="primary"
              onClick={() => setopenPopup1(true)}
            >
              <AddIcon />
              Thêm sinh viên tham gia
            </Button>
          )}
        </div>
      </GridItem>
      <GridItem xs={12} sm={12} md={12}>
        {detailEvent.status === "Đã Đăng ký" ? (
          <Table
            tableHeaderColor="primary"
            tableHead={[
              "Tên",
              "Mã sinh viên",
              "Lớp - Khoá",
              "Khoa",
              "Thời gian đăng ký",
              "Hành động",
            ]}
            tableData={table}
          />
        ) : (
          <Table
            tableHeaderColor="primary"
            tableHead={[
              "Tên",
              "Mã sinh viên",
              "Lớp - Khoá",
              "Khoa",
              "Thời gian tham gia",
              "Hành động",
            ]}
            tableData={table}
          />
        )}
      </GridItem>
      <div className={classes.container} style={{ marginTop: "10px" }}>
        <Button onClick={() => setopenPopup(false)} round>
          Đóng
        </Button>
      </div>
      <Popup
        openPopup={openPopup1}
        title={
          detailEvent.status === "Đã Đăng ký"
            ? "Thêm sinh viên đăng ký sự kiện"
            : "Thêm sinh viên tham gia sự kiện"
        }
      >
        <EventDetailAdd
          setopenPopup={setopenPopup1}
          detail={{ id: detail.id, status: detailEvent.status }}
        />
      </Popup>
    </GridContainer>
  );
}

export default EventDetail;
