import { makeStyles } from "@material-ui/core";
import { container } from "assets/jss/material-dashboard-react.js";
import Button from "components/CustomButtons/Button";
import CustomInput from "components/CustomInput/CustomInput";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import { Formik } from "formik";
import queryString from "query-string";
import React from "react";
import { store } from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import { useDispatch } from "react-redux";
import { userActions } from "Redux/Actions";
import * as yup from "yup";

const styles = {
  container,
};
const useStyles = makeStyles(styles);

function ChangePassword(props) {
  //!Const
  const classes = useStyles();
  const dispatch = useDispatch();
  //!State
  const [changePass, setstate] = React.useState({
    oldPassword: "",
    newPassword: "",
    newPasswordRetype: "",
  });
  const passSchema = yup.object().shape({
    oldPassword: yup.string().required("Vui lòng nhập mật khẩu hiện tại!"),
    newPassword: yup
      .string()
      .required("Vui lòng nhập mật khẩu mới!")
      .matches(
        "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{8,}$",
        "Mật khẩu phải có ít nhất 8 kí tự, trong đó chứa ít nhất 1 kí tự và một số!"
      ),
    newPasswordRetype: yup
      .string()
      .oneOf([yup.ref("newPassword")], "Mật khẩu không trùng khớp!")
      .required("Vui lòng xác nhận mật khẩu!"),
  });
  //!Function
  const handleChange = (value) => {
    const password = {
      oldPassword: value.oldPassword,
      newPassword: value.newPassword,
    };
    dispatch(
      userActions.changePassword(
        password,
        queryString.stringify({ token: localStorage.getItem("token") }),
        {
          success: () => {
            store.addNotification({
              title: "Thông báo!",
              message: "Cập nhật mật khẩu thành công!",
              type: "success", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                pauseOnHover: true,
                onScreen: true,
                duration: 3000,
              },
            });
            props.setopenPopup(false);
          },
          failed: (mess) => {
            store.addNotification({
              title: "Thông báo!",
              message: "Cập nhật mật khẩu thất bại! Lỗi: " + mess + "!",
              type: "warning", // 'default', 'success', 'info', 'warning'
              container: "bottom-right", // where to position the notifications
              animationIn: ["animated", "fadeIn"], // animate.css classes that's applied
              animationOut: ["animated", "fadeOut"], // animate.css classes that's applied
              dismiss: {
                onScreen: true,
                pauseOnHover: true,
                duration: 5000,
              },
            });
          },
        }
      )
    );
  };
  return (
    <GridContainer>
      <GridItem xs={12} sm={12} md={12}>
        <Formik
          enableReinitialize
          initialValues={changePass}
          validationSchema={passSchema}
          onSubmit={handleChange}
        >
          {({ values, setFieldValue, handleSubmit, errors, touched }) => {
            return (
              <GridContainer>
                <GridItem xs={12} sm={12} md={12}>
                  <CustomInput
                    id="oldPassword"
                    labelText="Mật khẩu hiện tại"
                    // value={values.address}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("oldPassword", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      //   autoComplete: "on",
                      autoComplete: "off",
                      type: "password",
                    }}
                  />
                  {errors.oldPassword && touched.oldPassword && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.oldPassword}
                    </a>
                  )}
                  <CustomInput
                    id="newPassword"
                    labelText="Mật khẩu mới"
                    // value={values.address}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("newPassword", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      //   autoComplete: "on",
                      autoComplete: "off",
                      type: "password",
                    }}
                  />
                  {errors.newPassword && touched.newPassword && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.newPassword}
                    </a>
                  )}
                  <CustomInput
                    id="newPasswordRetype"
                    labelText="Xác nhận mật khẩu mới"
                    // value={values.address}
                    //   style={{ color: "white" }}
                    onChange={(text) =>
                      setFieldValue("newPasswordRetype", text.target.value)
                    }
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      //   autoComplete: "on",
                      autoComplete: "off",
                      type: "password",
                    }}
                  />
                  {errors.newPasswordRetype && touched.newPasswordRetype && (
                    <a
                      style={{
                        fontSize: 12,
                        color: "red",
                      }}
                    >
                      {errors.newPasswordRetype}
                    </a>
                  )}
                  <div
                    className={classes.container}
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "15px",
                    }}
                  >
                    <Button color="success" round onClick={handleSubmit}>
                      Cập nhật
                    </Button>
                    &nbsp;
                    <Button round onClick={() => props.setopenPopup(false)}>
                      Huỷ
                    </Button>
                  </div>
                </GridItem>
              </GridContainer>
            );
          }}
        </Formik>
      </GridItem>
    </GridContainer>
  );
}

export default ChangePassword;
