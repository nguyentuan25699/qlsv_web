import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormHelperText,
  InputAdornment,
  makeStyles,
  Slide,
} from "@material-ui/core";
import { Close } from "@material-ui/icons";
import Face from "@material-ui/icons/Face";
import { primaryColor } from "assets/jss/material-dashboard-react";
import forgotpageStyle from "assets/jss/material-dashboard-react/views/forgotpageStyle";
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";
import CardHeader from "components/Card/CardHeader";
import Button from "components/CustomButtons/Button";
import CustomInput from "components/CustomInput/CustomInput";
import Footer from "components/Footer/Footer";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import React, { useState } from "react";
import ReactLoading from "react-loading";
import { useDispatch } from "react-redux";
import { userActions } from "Redux/Actions";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="down" ref={ref} {...props} />;
});

Transition.displayName = "Transition";
const useStyle = makeStyles(forgotpageStyle);
function ForgotPage(props) {
  //!Const
  const classes = useStyle();
  const dispatch = useDispatch();
  //!State
  const [forgot, setforgot] = useState({
    studentCode: "",
  });
  const [smallModal, setSmallModal] = React.useState(false);
  const [message, setmessage] = useState("");
  const [isforgoting, setisforgoting] = useState(false);
  //!Function
  const handleForgotPass = () => {
    setisforgoting(true);

    dispatch(
      userActions.forgotPassword(forgot, {
        success: () => {
          setmessage("");
          setisforgoting(false);
          setSmallModal(true);
        },
        failed: (mess) => {
          setisforgoting(false);
          setmessage(mess);
        },
      })
    );
  };
  return (
    // <div style={{ width: width }}>
    <div
      className={classes.pageHeaderWhite}
      style={{
        backgroundColor: "white",
        backgroundSize: "cover",
        backgroundPosition: "top center",
      }}
    >
      <div
        className={classes.container}
        style={{ marginRight: "auto", marginLeft: "auto" }}
      >
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={12}>
            {/* SMALL MODAL START */}
            <Dialog
              classes={{
                root: classes.modalRoot,
                paper: classes.modal + " " + classes.modalSmall,
              }}
              open={smallModal}
              TransitionComponent={Transition}
              keepMounted
              onClose={() => setSmallModal(false)}
              aria-labelledby="small-modal-slide-title"
              aria-describedby="small-modal-slide-description"
            >
              <DialogTitle
                id="small-modal-slide-title"
                disableTypography
                className={classes.modalHeader}
              >
                <div className={classes.container} style={{ display: "flex" }}>
                  <h3 style={{ color: "red" }}>
                    <b>Thông báo!</b>
                  </h3>
                  <Button
                    simple
                    className={classes.modalCloseButton}
                    key="close"
                    aria-label="Close"
                    onClick={() => setSmallModal(false)}
                  >
                    <Close className={classes.modalClose} />
                  </Button>
                </div>
              </DialogTitle>
              <DialogContent
                id="small-modal-slide-description"
                className={classes.modalBody + " " + classes.modalSmallBody}
              >
                <h5>
                  Link cấp lại mật khẩu đã được gửi tới gmail của bạn! Vui lòng
                  kiểm tra!
                </h5>
              </DialogContent>
              <DialogActions
                className={
                  classes.modalFooter + " " + classes.modalFooterCenter
                }
              >
                <Button
                  onClick={() => setSmallModal(false)}
                  link
                  className={classes.modalSmallFooterFirstButton}
                >
                  Ok
                </Button>
              </DialogActions>
            </Dialog>
            {/* SMALL MODAL END */}
            <Card style={{ minWidth: "350px" }}>
              <CardHeader className={classes.cardHeader} color="primary">
                <h4>QUÊN MẬT KHẨU</h4>
              </CardHeader>
              <p className={classes.description + " " + classes.textCenter}>
                Vui lòng nhập thông tin
              </p>
              <CardBody>
                <div>
                  <CustomInput
                    id="userName"
                    value={forgot.studentCode}
                    onChange={(text) => {
                      setforgot({ ...forgot, studentCode: text.target.value });
                    }}
                    formControlProps={{
                      fullWidth: true,
                    }}
                    inputProps={{
                      placeholder: "Mã sinh viên",
                      type: "text",
                      startAdornment: (
                        <InputAdornment position="start">
                          <Face />
                        </InputAdornment>
                      ),
                      autoComplete: "on",
                    }}
                  />
                </div>
              </CardBody>
              <div className={classes.textCenter}>
                <div
                  className={classes.container}
                  style={{ justifyContent: "center" }}
                >
                  {message !== "" ? (
                    <FormHelperText
                      style={{
                        marginTop: -5,
                        color: "red",
                        textAlign: "center",
                      }}
                    >
                      {message}
                    </FormHelperText>
                  ) : null}
                </div>
                <div
                  className={classes.container}
                  style={{
                    justifyContent: "center",
                    paddingBottom: "10px",
                    display: "flex",
                  }}
                >
                  {isforgoting ? (
                    <ReactLoading
                      type="spin"
                      color="black"
                      height="30px"
                      width="30px"
                    />
                  ) : (
                    <Button
                      color="primary"
                      size="lg"
                      onClick={handleForgotPass}
                      // href={isloged ? "/" : null}
                      className={classes.navLink}
                    >
                      CẤP MẬT KHẨU
                    </Button>
                  )}
                  <br />
                </div>

                <div
                  className={classes.container}
                  style={{
                    justifyContent: "center",
                    paddingBottom: "10px",
                    display: "flex",
                  }}
                >
                  <span>
                    Hoặc{" "}
                    <a
                      href="/login"
                      style={{ fontStyle: "italic", color: primaryColor[0] }}
                    >
                      đăng nhập?
                    </a>
                  </span>
                  <br />
                </div>
              </div>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
      <Footer
        className={classes.footer}
        style={{ justifyContent: "flex-end" }}
      />
    </div>
  );
}

export default ForgotPage;
