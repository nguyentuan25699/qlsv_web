import { FormHelperText, InputAdornment, makeStyles } from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import Face from "@material-ui/icons/Face";
import loginpageStyle from "assets/jss/material-dashboard-react/views/loginpageStyle";
import Card from "components/Card/Card";
import CardBody from "components/Card/CardBody";
import Button from "components/CustomButtons/Button";
import CardHeader from "components/Card/CardHeader";
import CustomInput from "components/CustomInput/CustomInput";
import GridContainer from "components/Grid/GridContainer";
import GridItem from "components/Grid/GridItem";
import React, { useState } from "react";
import Footer from "components/Footer/Footer";
import { primaryColor } from "assets/jss/material-dashboard-react";
import { Redirect } from "react-router";
import { useDispatch } from "react-redux";
import { userActions } from "Redux/Actions";
import ReactLoading from "react-loading";

const useStyle = makeStyles(loginpageStyle);
function LoginPage(props) {
  //!Const
  const dispatch = useDispatch();
  //!State
  const [login, setlogin] = useState({
    studentCode: "",
    password: "",
  });
  const [message, setmessage] = useState("");
  const [isloged, setisloged] = useState(false);
  const [isLogging, setisLogging] = useState(false);
  const classes = useStyle();
  //!function
  const handleLogin = () => {
    setisLogging(true);
    dispatch(
      userActions.login(login, {
        success: () => {
          setisloged(true);
        },
        failed: (error) => {
          if (error.toString().includes("Incorrect studentCode or password")) {
            setmessage("Mã sinh viên hoặc mật khẩu không chính xác!");
          } else if (
            error
              .toString()
              .includes('"studentCode" is not allowed to be empty')
          ) {
            setmessage("Không được để trống mã sinh viên!");
          } else if (
            error.toString().includes('"password" is not allowed to be empty')
          ) {
            setmessage("Không được để trống mật khẩu!");
          }
          setisLogging(false);
        },
      })
    );
  };
  if (isloged) {
    return <Redirect to="/" />;
  }
  localStorage.setItem("token", "");
  localStorage.setItem("id", "");
  localStorage.setItem("role", "");
  localStorage.setItem("expiresAt", "");
  localStorage.setItem("refreshtoken", "");
  //!Render
  return (
    // <div style={{ width: width }}>
    <div
      className={classes.pageHeaderWhite}
      style={{
        backgroundColor: "white",
        backgroundSize: "cover",
        backgroundPosition: "top center",
      }}
    >
      <div
        className={classes.container}
        style={{ marginRight: "auto", marginLeft: "auto" }}
      >
        <GridContainer justify="center">
          <GridItem xs={12} sm={12} md={10}>
            <Card>
              <CardHeader className={classes.cardHeader} color="primary">
                <h4>ĐĂNG NHẬP HỆ THỐNG</h4>
              </CardHeader>
              <p className={classes.description + " " + classes.textCenter}>
                Vui lòng đăng nhập để sử dụng
              </p>
              <CardBody>
                <CustomInput
                  required
                  id="studentCode"
                  value={login.studentCode}
                  onChange={(text) => {
                    setlogin({ ...login, studentCode: text.target.value });
                  }}
                  formControlProps={{
                    fullWidth: true,
                  }}
                  inputProps={{
                    placeholder: "Tên tài khoản",
                    type: "text",
                    startAdornment: (
                      <InputAdornment position="start">
                        <Face />
                      </InputAdornment>
                    ),
                    autoComplete: "on",
                  }}
                />

                <CustomInput
                  required
                  id="pass"
                  value={login.password}
                  onChange={(text) => {
                    setlogin({ ...login, password: text.target.value });
                  }}
                  formControlProps={{
                    fullWidth: true,
                  }}
                  inputProps={{
                    placeholder: "Mật khẩu",
                    type: "password",
                    startAdornment: (
                      <InputAdornment position="start">
                        <Icon className={classes.inputIconsColor}>
                          lock_utline
                        </Icon>
                      </InputAdornment>
                    ),
                    autoComplete: "on",
                  }}
                />
              </CardBody>
              <div className={classes.textCenter}>
                <div
                  className={classes.container}
                  style={{ justifyContent: "center" }}
                >
                  {message !== "" ? (
                    <FormHelperText
                      style={{
                        marginTop: -5,
                        color: "red",
                        textAlign: "center",
                      }}
                    >
                      {message}
                    </FormHelperText>
                  ) : null}
                </div>
                <div
                  className={classes.container}
                  style={{
                    justifyContent: "center",
                    paddingBottom: "10px",
                    display: "flex",
                  }}
                >
                  {isLogging ? (
                    <ReactLoading
                      type="spin"
                      color="black"
                      height="30px"
                      width="30px"
                    />
                  ) : (
                    <Button
                      color="primary"
                      size="lg"
                      onClick={handleLogin}
                      // href={isloged ? "/" : null}
                      className={classes.navLink}
                    >
                      Đăng nhập
                    </Button>
                  )}
                  <br />
                </div>

                <div
                  className={classes.container}
                  style={{
                    justifyContent: "center",
                    paddingBottom: "10px",
                    display: "flex",
                  }}
                >
                  <span>
                    Hoặc{" "}
                    <a
                      href="forgot-password"
                      style={{ fontStyle: "italic", color: primaryColor[0] }}
                    >
                      quên mật khẩu?
                    </a>
                  </span>
                  <br />
                </div>
              </div>
            </Card>
          </GridItem>
        </GridContainer>
      </div>
      <Footer
        className={classes.footer}
        style={{ justifyContent: "flex-end" }}
      />
    </div>
  );
}

export default LoginPage;
