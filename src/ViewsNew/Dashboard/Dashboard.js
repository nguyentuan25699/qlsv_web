import Icon from "@material-ui/core/Icon";
// @material-ui/core
import { makeStyles } from "@material-ui/core/styles";
import Accessibility from "@material-ui/icons/Accessibility";
import EventAvailableIcon from "@material-ui/icons/EventAvailable";
import GroupIcon from "@material-ui/icons/Group";
import TodayIcon from "@material-ui/icons/Today";
import Update from "@material-ui/icons/Update";
import styles from "assets/jss/material-dashboard-react/views/dashboardStyle.js";
import Card from "components/Card/Card.js";
import CardFooter from "components/Card/CardFooter.js";
import CardHeader from "components/Card/CardHeader.js";
import CardIcon from "components/Card/CardIcon.js";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import React from "react";
import { useDispatch } from "react-redux";
import { eventActions } from "Redux/Actions";
import { userActions } from "Redux/Actions";
import teamActions from "Redux/Actions/teamActions";
import queryString from "query-string";
import CSVUpload from "components/CustomUpload/CSVUpload";

const useStyles = makeStyles(styles);

export default function Dashboard() {
  //!Const
  const dispatch = useDispatch();
  const classes = useStyles();
  //!State
  const [sumUser, setsumUser] = React.useState(0);
  const [sumTeam, setsumTeam] = React.useState(0);
  const [sumEvent, setsumEvent] = React.useState(0);
  const [sumEventGoing, setsumEventGoing] = React.useState(0);

  //!Use effect
  React.useEffect(() => {
    dispatch(
      userActions.getListUsers("", {
        success: (data) => {
          setsumUser(data.totalResults);
          dispatch(
            teamActions.getListTeam("", {
              success: (data) => {
                setsumTeam(data.totalResults);
                dispatch(
                  eventActions.getListEvent("", {
                    success: (data) => {
                      setsumEvent(data.totalResults);
                      dispatch(
                        eventActions.getListEvent(
                          queryString.stringify({ status: "Đang Diễn ra" }),
                          {
                            success: (data) => {
                              setsumEventGoing(data.totalResults);
                            },
                            failed: (mess) => {},
                          }
                        )
                      );
                    },
                    failed: (mess) => {},
                  })
                );
              },
              failed: (mess) => {},
            })
          );
        },
        failed: (mess) => {},
      })
    );
  }, []);
  return (
    <div>
      <GridContainer>
        <GridItem xs={12} sm={6} md={5}>
          <Card>
            <CardHeader color="info" stats icon>
              <CardIcon color="info">
                <Accessibility />
              </CardIcon>
              <p className={classes.cardCategory}>Tổng số sinh viên</p>
              <h3 className={classes.cardTitle}>{sumUser}</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Update />
                Vừa cập nhật
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={5}>
          <Card>
            <CardHeader color="danger" stats icon>
              <CardIcon color="danger">
                <Icon>
                  <GroupIcon />
                </Icon>
              </CardIcon>
              <p className={classes.cardCategory}>
                Tổng số Câu lạc bộ, Đội - Nhóm
              </p>
              <h3 className={classes.cardTitle}>{sumTeam}</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Update />
                Vừa cập nhật
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={5}>
          <Card>
            <CardHeader color="warning" stats icon>
              <CardIcon color="warning">
                <EventAvailableIcon />
              </CardIcon>
              <p className={classes.cardCategory}>Tổng số sự kiện</p>
              <h3 className={classes.cardTitle}>
                {/* 49/50 <small>GB</small> */}
                {sumEvent}
              </h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Update />
                Vừa cập nhật
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={6} md={5}>
          <Card>
            <CardHeader color="success" stats icon>
              <CardIcon color="success">
                <TodayIcon />
              </CardIcon>
              <p className={classes.cardCategory}>Số sự kiện đang diễn ra</p>
              <h3 className={classes.cardTitle}>{sumEventGoing}</h3>
            </CardHeader>
            <CardFooter stats>
              <div className={classes.stats}>
                <Update />
                Vừa cập nhật
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        {/* <GridItem xs={12} sm={6} md={5}>
          <CSVUpload
            files={(file) => {
              console.log({ file });
            }}
          />
        </GridItem> */}
      </GridContainer>
      {/* <GridContainer>
        <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="success">
              <ChartistGraph
                className="ct-chart"
                data={dailySalesChart.data}
                type="Line"
                options={dailySalesChart.options}
                listener={dailySalesChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Daily Sales</h4>
              <p className={classes.cardCategory}>
                <span className={classes.successText}>
                  <ArrowUpward className={classes.upArrowCardCategory} /> 55%
                </span>{" "}
                increase in today sales.
              </p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> updated 4 minutes ago
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="warning">
              <ChartistGraph
                className="ct-chart"
                data={emailsSubscriptionChart.data}
                type="Bar"
                options={emailsSubscriptionChart.options}
                responsiveOptions={emailsSubscriptionChart.responsiveOptions}
                listener={emailsSubscriptionChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Email Subscriptions</h4>
              <p className={classes.cardCategory}>Last Campaign Performance</p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> campaign sent 2 days ago
              </div>
            </CardFooter>
          </Card>
        </GridItem>
        <GridItem xs={12} sm={12} md={4}>
          <Card chart>
            <CardHeader color="danger">
              <ChartistGraph
                className="ct-chart"
                data={completedTasksChart.data}
                type="Line"
                options={completedTasksChart.options}
                listener={completedTasksChart.animation}
              />
            </CardHeader>
            <CardBody>
              <h4 className={classes.cardTitle}>Completed Tasks</h4>
              <p className={classes.cardCategory}>Last Campaign Performance</p>
            </CardBody>
            <CardFooter chart>
              <div className={classes.stats}>
                <AccessTime /> campaign sent 2 days ago
              </div>
            </CardFooter>
          </Card>
        </GridItem>
      </GridContainer> */}
      {/* <GridContainer>
        <GridItem xs={12} sm={12} md={6}>
          <CustomTabs
            title="Tasks:"
            headerColor="primary"
            tabs={[
              {
                tabName: "Bugs",
                tabIcon: BugReport,
                tabContent: (
                  <Tasks
                    checkedIndexes={[0, 3]}
                    tasksIndexes={[0, 1, 2, 3]}
                    tasks={bugs}
                  />
                ),
              },
              {
                tabName: "Website",
                tabIcon: Code,
                tabContent: (
                  <Tasks
                    checkedIndexes={[0]}
                    tasksIndexes={[0, 1]}
                    tasks={website}
                  />
                ),
              },
              {
                tabName: "Server",
                tabIcon: Cloud,
                tabContent: (
                  <Tasks
                    checkedIndexes={[1]}
                    tasksIndexes={[0, 1, 2]}
                    tasks={server}
                  />
                ),
              },
            ]}
          />
        </GridItem>
        <GridItem xs={12} sm={12} md={6}>
          <Card>
            <CardHeader color="warning">
              <h4 className={classes.cardTitleWhite}>Employees Stats</h4>
              <p className={classes.cardCategoryWhite}>
                New employees on 15th September, 2016
              </p>
            </CardHeader>
            <CardBody>
              <Table
                tableHeaderColor="warning"
                tableHead={["ID", "Name", "Salary", "Country"]}
                tableData={[
                  ["1", "Dakota Rice", "$36,738", "Niger"],
                  ["2", "Minerva Hooper", "$23,789", "Curaçao"],
                  ["3", "Sage Rodriguez", "$56,142", "Netherlands"],
                  ["4", "Philip Chaney", "$38,735", "Korea, South"],
                ]}
              />
            </CardBody>
          </Card>
        </GridItem>
      </GridContainer> */}
    </div>
  );
}
