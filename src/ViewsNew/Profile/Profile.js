// @material-ui/core components
import { makeStyles } from "@material-ui/core/styles";
import { container } from "assets/jss/material-dashboard-react.js";
import Card from "components/Card/Card.js";
import CardAvatar from "components/Card/CardAvatar.js";
import CardBody from "components/Card/CardBody.js";
import Button from "components/CustomButtons/Button.js";
import GridContainer from "components/Grid/GridContainer.js";
// core components
import GridItem from "components/Grid/GridItem.js";
import Popup from "components/Popup/Popup";
import moment from "moment";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { teamActions } from "Redux/Actions";
import { BASE_URL_IMAGE } from "Service/ServiceURL";
import UserManageAdd from "ViewsNew/UserManage/UserManageAdd";
import queryString from "query-string";

const styles = {
  container,
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0",
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
  },
  left: {
    float: "left!important",
    display: "block",
    fontWeight: "300",
    fontSize: "18px",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    padding: "0",
    marginTop: "0",
  },
  right: {
    padding: "15px 0",
    margin: "0",
    fontSize: "14px",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    float: "right!important",
  },
};

const useStyles = makeStyles(styles);
function Profile(props) {
  //!State
  const [openPopup, setopenPopup] = React.useState(false);
  const [isDone, setisDone] = React.useState(false);
  const [listTeamName, setlistTeamName] = React.useState("");
  const dispatch = useDispatch();
  //!Const
  const userDetail = useSelector((state) => state.userReducer.userByID);
  //!Use effect
  React.useEffect(() => {
    dispatch(
      teamActions.getListTeam(queryString.stringify({ limit: 100000 }), {
        success: (data) => {
          let stringArray = [];
          let string = "";
          data.results.map((index) => {
            if (userDetail.teamID !== undefined) {
              userDetail.teamID.map((index1) => {
                if (index.id === index1) {
                  stringArray.push(index);
                }
              });
            }
          });
          for (let i = 0; i < stringArray.length; i++) {
            if (i < stringArray.length - 1) {
              string = string + stringArray[i].name + ", ";
            } else {
              string = string + stringArray[i].name + ".";
            }
          }
          setlistTeamName(string);
        },
        failed: (failed) => {},
      })
    );
  }, [userDetail]);
  const classes = useStyles();
  return (
    <div>
      <GridContainer justify="center">
        <GridItem xs={12} sm={12} md={8}>
          <Card profile>
            <CardAvatar profile>
              <a href="#pablo" onClick={(e) => e.preventDefault()}>
                <img
                    style={{ width: "200px", height: "200px" }}
                  src={BASE_URL_IMAGE + userDetail.image}
                  alt="..."
                />
              </a>
            </CardAvatar>
            <CardBody profile>
              <h2 className={classes.cardCategory}>
                <b>{userDetail.name}</b>
              </h2>
              <h4 className={classes.cardTitle}>
                <b>Quyền sử dụng: </b>
                <a
                  style={{
                    textTransform: "uppercase",
                    color: "black",
                  }}
                >
                  {userDetail.role}
                </a>
              </h4>
              <GridContainer>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>Mã sinh viên:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>{userDetail.studentCode}</p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>Lớp - Khoá:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      {userDetail.class + " - K" + userDetail.schoolYear}
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>Khoa:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>{userDetail.faculty}</p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>Ngày sinh:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      {moment(userDetail.dateOfBird).format("DD/MM/YYYY")}
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>Giới tính:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>{userDetail.gender}</p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>Địa chỉ:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>{userDetail.address}</p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>Email:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>{userDetail.email}</p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>Số điện thoại:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>{userDetail.phoneNumber}</p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>
                      <b>CLB, đội, nhóm tham gia:</b>
                    </p>
                  </div>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                  <div className={container}>
                    <p className={classes.left}>{listTeamName}</p>
                  </div>
                </GridItem>
              </GridContainer>
              <Button color="primary" roun onClick={() => setopenPopup(true)}>
                Cập nhật thông tin cá nhân
              </Button>
            </CardBody>
          </Card>
        </GridItem>
        <Popup openPopup={openPopup} title="Cập nhật tài khoản cá nhân">
          <UserManageAdd
            setisDone={setisDone}
            setopenPopup={setopenPopup}
            detail={userDetail}
            isPersonal={true}
          />
        </Popup>
      </GridContainer>
    </div>
  );
}

export default Profile;
